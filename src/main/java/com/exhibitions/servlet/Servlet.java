package com.exhibitions.servlet;

import com.exhibitions.command.CommandManager;
import com.exhibitions.command.LoginCommand;
import com.exhibitions.command.RegisterCommand;
import com.exhibitions.command.ServletCommand;
import com.exhibitions.command.admin.exhibition.UpdateExhibitionAdminCommand;
import org.apache.log4j.Logger;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/Servlet")
public class Servlet extends HttpServlet {

    private static final Logger LOGGER = Logger.getLogger(Servlet.class);

    private CommandManager commandManager;

    public void init(ServletConfig config) throws ServletException {
        LOGGER.info("Initializing Servlet");
        commandManager = new CommandManager();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        LOGGER.info("Processing get request");

        ServletCommand command = commandManager.getGetCommand(request);

        execute(request, response, command);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        LOGGER.info("Processing post request");

        ServletCommand command = commandManager.getPostCommand(request);

        execute(request, response, command);
    }

    private void execute(HttpServletRequest request, HttpServletResponse response, ServletCommand command) throws ServletException, IOException {
        if (request.getParameter("locale") != null) {
            String requestLocale = request.getParameter("locale");

            switch (requestLocale) {
                case "en":
                    request.getSession().setAttribute("locale", "en");
                    break;
                case "ru":
                    request.getSession().setAttribute("locale", "ru");
                    break;
            }
        }
        String page = "";

        if ("ru".equals(request.getSession().getAttribute("locale")))
            page = command.execute(request, response, "ru");
        else
            page = command.execute(request, response, "en");
        if (command instanceof RegisterCommand)
            response.sendRedirect("/exhibitions_war_exploded");
        else if (command instanceof UpdateExhibitionAdminCommand)
            response.sendRedirect("/exhibitions_war_exploded/admin/exhibitions?p=1&s=10");
        else
            request.getRequestDispatcher(page).forward(request, response);
        // response.sendRedirect(page);
    }

}
