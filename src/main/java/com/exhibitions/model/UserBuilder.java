package com.exhibitions.model;

public class UserBuilder {
    private Long id;
    private String name;
    private String name_ukr;
    private String email;
    private String password;
    private UserType userType;

    public UserBuilder setId(Long id) {
        this.id = id;
        return this;
    }

    public UserBuilder setName(String name) {
        this.name = name;
        return this;
    }

    public UserBuilder setEmail(String email) {
        this.email = email;
        return this;
    }

    public UserBuilder setPassword(String password) {
        this.password = password;
        return this;
    }

    public UserBuilder setUserType(UserType userType) {
        this.userType = userType;
        return this;
    }

    public UserBuilder setName_ukr(String name_ukr) {
        this.name_ukr = name_ukr;
        return this;
    }

    public User build() {return new User(id, name, name_ukr, email, password, userType);}
}
