package com.exhibitions.model;

import java.util.Objects;

public class Hall {

    private long id;
    private String title;
    private String title_ukr;

    public Hall() {
    }

    public Hall(long id, String title, String title_ukr) {
        this.id = id;
        this.title = title;
        this.title_ukr = title_ukr;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle_ukr() {
        return title_ukr;
    }

    public void setTitle_ukr(String title_ukr) {
        this.title_ukr = title_ukr;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Hall hall = (Hall) o;
        return id == hall.id && Objects.equals(title, hall.title) && Objects.equals(title_ukr, hall.title_ukr);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, title, title_ukr);
    }
}
