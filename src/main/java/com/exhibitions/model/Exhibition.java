package com.exhibitions.model;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;
import java.util.Objects;

public class Exhibition implements Serializable {

    private Long id;
    private String title;
    private String title_ukr;
    private Category category;
    private Float price;
    private String description;
    private String description_ukr;
    private LocalDate startDate;
    private LocalDate endDate;
    private List<Hall> halls;

    public Exhibition(Long id, String title, String title_ukr, Category category, Float price,
                      String description, String description_ukr, LocalDate startDate, LocalDate endDate, List<Hall> halls) {
        this.id = id;
        this.title = title;
        this.title_ukr = title_ukr;
        this.category = category;
        this.price = price;
        this.description = description;
        this.description_ukr = description_ukr;
        this.startDate = startDate;
        this.endDate = endDate;
        this.halls = halls;
    }

    public Exhibition() {}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public List<Hall> getHalls() {
        return halls;
    }

    public void setHalls(List<Hall> halls) {
        this.halls = halls;
    }

    public String getTitle_ukr() {
        return title_ukr;
    }

    public void setTitle_ukr(String title_ukr) {
        this.title_ukr = title_ukr;
    }

    public String getDescription_ukr() {
        return description_ukr;
    }

    public void setDescription_ukr(String description_ukr) {
        this.description_ukr = description_ukr;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Exhibition that = (Exhibition) o;
        return id.equals(that.id) && title.equals(that.title) && category.equals(that.category) && price.equals(that.price) && description.equals(that.description) && startDate.equals(that.startDate) && endDate.equals(that.endDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, title, category, price, description, startDate, endDate);
    }
}
