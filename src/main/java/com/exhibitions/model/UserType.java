package com.exhibitions.model;

import java.io.Serializable;

public enum UserType implements Serializable {
    ADMIN,
    USER
}
