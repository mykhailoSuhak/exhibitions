package com.exhibitions.model;

import java.time.LocalDate;
import java.util.List;

public class ExhibitionBuilder {
    private Long id;
    private String title;
    private String title_ukr;
    private Category category;
    private Float price;
    private String description;
    private String description_ukr;
    private LocalDate startDate;
    private LocalDate endDate;
    private List<Hall> halls;

    public ExhibitionBuilder setId(Long id) {
        this.id = id;
        return this;
    }

    public ExhibitionBuilder setTitle(String title) {
        this.title = title;
        return this;

    }

    public ExhibitionBuilder setCategory(Category category) {
        this.category = category;
        return this;

    }

    public ExhibitionBuilder setPrice(Float price) {
        this.price = price;
        return this;

    }

    public ExhibitionBuilder setDescription(String description) {
        this.description = description;
        return this;

    }

    public ExhibitionBuilder setStartDate(LocalDate startDate) {
        this.startDate = startDate;
        return this;

    }

    public ExhibitionBuilder setEndDate(LocalDate endDate) {
        this.endDate = endDate;
        return this;

    }

    public ExhibitionBuilder setTitle_ukr(String title_ukr) {
        this.title_ukr = title_ukr;
        return this;
    }

    public ExhibitionBuilder setDescription_ukr(String description_ukr) {
        this.description_ukr = description_ukr;
        return this;
    }

    public ExhibitionBuilder setHalls(List<Hall> halls) {
        this.halls = halls;
        return this;
    }

    public Exhibition build() {return new Exhibition(id, title, title_ukr, category, price, description, description_ukr, startDate, endDate, halls);}

}
