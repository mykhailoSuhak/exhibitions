package com.exhibitions.model;

import java.io.Serializable;
import java.sql.Date;
import java.util.Objects;

public class Ticket implements Serializable {
    private Long id;
    private User user;
    private Exhibition exhibition;
    private Float price;

    public Ticket() {
    }

    public Ticket(Long id, User user, Exhibition exhibition, Float price) {
        this.id = id;
        this.user = user;
        this.exhibition = exhibition;
        this.price = price;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Exhibition getExhibition() {
        return exhibition;
    }

    public void setExhibition(Exhibition exhibition) {
        this.exhibition = exhibition;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Ticket ticket = (Ticket) o;
        return Objects.equals(id, ticket.id) && Objects.equals(user, ticket.user) && Objects.equals(exhibition, ticket.exhibition) && Objects.equals(price, ticket.price);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, user, exhibition, price);
    }
}


