package com.exhibitions.model;

import java.util.Objects;

public class Category {
    private long id;
    private String name;
    private String name_ukr;


    public Category() {}

    public Category(long id, String name, String name_ukr) {
        this.id = id;
        this.name = name;
        this.name_ukr = name_ukr;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName_ukr() {
        return name_ukr;
    }

    public void setName_ukr(String name_ukr) {
        this.name_ukr = name_ukr;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Category category = (Category) o;
        return id == category.id && Objects.equals(name, category.name) && Objects.equals(name_ukr, category.name_ukr);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, name_ukr);
    }
}