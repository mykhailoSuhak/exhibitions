package com.exhibitions.dao.hall;

import com.exhibitions.model.Hall;

import java.util.List;

public interface HallDao {
    /**
     * This method saves new hall to db.
     *
     * @param hall Object to be created.
     * @return          An updated object.
     */
    Hall createHall(Hall hall);

    /**
     * This method updates hall.
     *
     * @param hall Object to be updated.
     * @return          An updated object.
     */
    Hall updateHall(Hall hall);

    /**
     * This method deletes hall from db.
     *
     * @param id Id of the hall to be deleted.
     * @return   True if deletion successful, otherwise false.
     */
    boolean deleteHallById(Long id);

    /**
     * This method finds hall by id.
     *
     * @param id Id of the hall.
     * @return   Hall object retrieved from db, otherwise null.
     */
    Hall findHallById(Long id);

    /**
     * This method gets all halls
     *
     * @return A list of all halls.
     */
    List<Hall> findAll(String locale);

    /**
     * This method gets list of halls applied to exhibition
     *
     * @param id Id of the exhibition.
     * @return A list of all halls.
     */
    List<Hall> findHallsByExhibitionId(long id, String locale);

    /**
     * This method removes all halls for exhibition.
     *
     * @param exhibitionId Id of the exhibition.
     * @return     True if halls were deleted successfully, otherwise false.
     */
    public boolean removeHallsForExhibition(Long exhibitionId);

    /**
     * This method finds hall by title.
     *
     * @param title Title of the hall.
     * @return     Hall object with given title, otherwise null.
     */
    Hall findHallByTitle(String title);

    /**
     * This method gets all available halls for the time range.
     *
     * @param startDate start of the period.
     * @param endDate end of the period.
     * @return A list of all halls.
     */
    List<Hall> findAvailableHalls(String startDate, String endDate);

    /**
     * This method sets halls for exhibition.
     *
     * @param halls List of halls to be set.
     * @param exhibitionId Id of the exhibition.
     * @return True if halls are set successfully, otherwise false.
     */
    boolean setHallsForExhibition(Long exhibitionId, List<Hall> halls);
}
