package com.exhibitions.dao.hall;

import com.exhibitions.connection.ConnectionPool;
import com.exhibitions.model.Hall;
import com.exhibitions.properties.MysqlQueryProperties;
import org.apache.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class MysqlHallDaoImpl implements HallDao{
    private static final Logger LOGGER = Logger.getLogger(MysqlHallDaoImpl.class);

    private static MysqlHallDaoImpl INSTANCE;
    private static ConnectionPool connectionPool;

    private static String createQuery;
    private static String updateQuery;
    private static String deleteQuery;
    private static String findByIdQuery;
    private static String findByNameQuery;
    private static String findAllQuery;
    private static String findHallsForExhibitionQuery;
    private static String setHallsQuery;
    private static String removeHallsForExhibition;


    private MysqlHallDaoImpl() {
        LOGGER.info("Initializing MysqlHallDaoImpl");

        connectionPool = ConnectionPool.getInstance();
        MysqlQueryProperties properties = MysqlQueryProperties.getInstance();

        createQuery = properties.getProperty("createHall");
        updateQuery = properties.getProperty("updateHallById");
        deleteQuery = properties.getProperty("deleteHallById");
        findByIdQuery = properties.getProperty("findHallById");
        findAllQuery = properties.getProperty("findAllHalls");
        findByNameQuery = properties.getProperty("findHallByName");
        findHallsForExhibitionQuery = properties.getProperty("findHallsForExhibition");
        setHallsQuery = properties.getProperty("setHallsQuery");
        removeHallsForExhibition = properties.getProperty("removeHallsForExhibition");
    }

    public static MysqlHallDaoImpl getInstance() {
        if(INSTANCE == null) {
            INSTANCE = new MysqlHallDaoImpl();
        }
        return INSTANCE;
    }

        @Override
    public Hall createHall(Hall hall) {
            LOGGER.info("Creating new hall");

            try(Connection connection = connectionPool.getConnection()) {
                PreparedStatement statement = connection.prepareStatement(createQuery, Statement.RETURN_GENERATED_KEYS);
                statement.setString(1, hall.getTitle());
                statement.setString(2, hall.getTitle_ukr());

                int affectedRows = statement.executeUpdate();

                if(affectedRows == 0) {
                    LOGGER.info("Hall creation failed");
                }
                else {
                    LOGGER.info("Hall creation successful");

                    try (ResultSet generatedKeys = statement.getGeneratedKeys()) {
                        if (generatedKeys.next()) {
                            hall.setId(generatedKeys.getLong(1));
                        }
                        else {
                            LOGGER.error("Failed to create hall, no ID obtained.");
                        }
                    }
                }
            } catch (SQLException e) {
                LOGGER.error(e.getMessage());
            }

            return hall;
    }

    @Override
    public Hall updateHall(Hall hall) {
        LOGGER.info("Updating hall");

        try(Connection connection = connectionPool.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(updateQuery);
            statement.setString(1, hall.getTitle());
            statement.setString(2, hall.getTitle_ukr());
            statement.setLong(3, hall.getId());

            boolean result = statement.execute();

            if(result) {
                LOGGER.info("Hall update failed");
            }
            else {
                LOGGER.info("Hall updated successfully");
            }
        } catch (SQLException e) {
            LOGGER.error(e.getMessage());
        }

        return hall;
    }

    @Override
    public boolean deleteHallById(Long id) {
        LOGGER.info("Deleting hall");
        boolean res = false;

        try(Connection connection = connectionPool.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(deleteQuery);
            statement.setLong(1, id);

            boolean result = statement.execute();

            if(result) {
                LOGGER.info("Hall deletion failed");
            }
            else {
                LOGGER.info("Hall deleted successfully");
                res = true;
            }
        } catch (SQLException e) {
            LOGGER.error(e.getMessage());
        }

        return res;
    }

    @Override
    public Hall findHallById(Long id) {
        LOGGER.info("Getting hall with id " + id);
        Hall hall = null;

        try(Connection connection = connectionPool.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(findByIdQuery);
            statement.setLong(1, id);

            ResultSet result = statement.executeQuery();

            if(result.next()) {
                hall = new Hall();
                hall.setId(result.getLong("id"));
                hall.setTitle(result.getString("name"));
                hall.setTitle_ukr(result.getString("name_ukr"));
            }
        } catch (SQLException e) {
            LOGGER.error(e.getMessage());
        }

        return hall;
    }

    @Override
    public List<Hall> findAll(String locale) {
        LOGGER.info("Getting all halls");
        List<Hall> res = new ArrayList<>();

        try(Connection connection = connectionPool.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(findAllQuery);
            ResultSet result = statement.executeQuery();

            while(result.next()) {
                Hall hall = new Hall();
                hall.setId(result.getLong("id"));
                if ("ru".equals(locale))
                    hall.setTitle(result.getString("name_ukr"));
                else
                    hall.setTitle(result.getString("name"));

                res.add(hall);
            }
        } catch (SQLException e) {
            LOGGER.error(e.getMessage());
        }

        return res;
    }

    @Override
    public List<Hall> findHallsByExhibitionId(long id, String locale) {
        LOGGER.info("Getting halls for exhibition with id " + id);
        List<Hall> res = new ArrayList<>();

        try(Connection connection = connectionPool.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(findHallsForExhibitionQuery);
            statement.setLong(1, id);
            ResultSet result = statement.executeQuery();

            while(result.next()) {
                Hall hall = new Hall();
                hall.setId(result.getLong("id"));
                if ("ru".equals(locale))
                    hall.setTitle(result.getString("name_ukr"));
                else
                    hall.setTitle(result.getString("name"));

                res.add(hall);
            }
        } catch (SQLException e) {
            LOGGER.error(e.getMessage());
        }

        return res;
    }

    @Override
    public boolean removeHallsForExhibition(Long exhibitionId) {
        LOGGER.info("Setting halls for exhibition");
        boolean res = false;

        try(Connection connection = connectionPool.getConnection()) {


            PreparedStatement statement = connection.prepareStatement(removeHallsForExhibition);
            statement.setLong(1, exhibitionId);

            boolean result = statement.execute();

            if (result) {
                LOGGER.info("Hall removal failed");
            } else {
                LOGGER.info("Hall removal successful");
                res = true;
            }

        } catch (SQLException e) {
            LOGGER.error(e.getMessage());
        }

        return res;
    }

    @Override
    public Hall findHallByTitle(String title) {
        return null;
    }

    @Override
    public List<Hall> findAvailableHalls(String startDate, String endDate) {
        return null;
    }

    @Override
    public boolean setHallsForExhibition(Long exhibitionId, List<Hall> halls) {
        LOGGER.info("Setting halls for exhibition");
        boolean res = false;

        try(Connection connection = connectionPool.getConnection()) {

            for (Hall hall: halls) {
                PreparedStatement statement = connection.prepareStatement(setHallsQuery);
                statement.setLong(1, hall.getId());
                statement.setLong(2, exhibitionId);

                boolean result = statement.execute();

                if (result) {
                    LOGGER.info("Hall set failed");
                } else {
                    LOGGER.info("Hall set successfully");
                    res = true;
                }
            }
        } catch (SQLException e) {
            LOGGER.error(e.getMessage());
        }

        return res;
    }
}
