package com.exhibitions.dao.ticket;

import com.exhibitions.connection.ConnectionPool;
import com.exhibitions.dao.exhibition.ExhibitionDao;
import com.exhibitions.dao.exhibition.MysqlExhibitionDaoImpl;
import com.exhibitions.dao.user.MysqlUserDaoImpl;
import com.exhibitions.dao.user.UserDao;
import com.exhibitions.model.Ticket;
import com.exhibitions.properties.MysqlQueryProperties;
import org.apache.log4j.Logger;

import java.math.BigDecimal;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class MysqlTicketDaoImpl implements TicketDao {

    private static final Logger LOGGER = Logger.getLogger(MysqlTicketDaoImpl.class);

    private static MysqlTicketDaoImpl INSTANCE;
    private static ConnectionPool connectionPool;
    private ExhibitionDao exhibitionDao;
    private UserDao userDao;

    private static String createQuery;
    private static String updateQuery;
    private static String deleteQuery;
    private static String findByIdQuery;
    private static String checkIfUserHasTicketQuery;
    private static String findByUserIdQuery;
    private static String findPageQuery;
    private static String findSoldTickets;

    private MysqlTicketDaoImpl() {
        LOGGER.info("Initializing MysqlTicketDaoImpl");

        connectionPool = ConnectionPool.getInstance();
        MysqlQueryProperties properties = MysqlQueryProperties.getInstance();

        createQuery = properties.getProperty("createTicket");
        updateQuery = properties.getProperty("updateTicketById");
        deleteQuery = properties.getProperty("deleteTicketById");
        findByIdQuery = properties.getProperty("findTicketById");
        checkIfUserHasTicketQuery = properties.getProperty("checkIfUserHasTicket");
        findByUserIdQuery = properties.getProperty("findTicketsByUserId");
        findPageQuery = properties.getProperty("findTicketPage");
        findSoldTickets = properties.getProperty("findSoldTickets");
        exhibitionDao = MysqlExhibitionDaoImpl.getInstance();
        userDao = MysqlUserDaoImpl.getInstance();
    }

    public static MysqlTicketDaoImpl getInstance() {
        if(INSTANCE == null) {
            INSTANCE = new MysqlTicketDaoImpl();
        }
        return INSTANCE;
    }

    @Override
    public Ticket createTicket(Ticket ticket) {
        LOGGER.info("Creating new ticket");

        try(Connection connection = connectionPool.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(createQuery, Statement.RETURN_GENERATED_KEYS);
            statement.setLong(1, ticket.getUser().getId());
            statement.setLong(2, ticket.getExhibition().getId());
            statement.setBigDecimal(3, new BigDecimal(ticket.getPrice()));

            int affectedRows = statement.executeUpdate();

            if(affectedRows == 0) {
                LOGGER.info("Ticket creation failed");
            }
            else {
                LOGGER.info("Ticket creation successful");

                try (ResultSet generatedKeys = statement.getGeneratedKeys()) {
                    if (generatedKeys.next()) {
                        ticket.setId(generatedKeys.getLong(1));
                    }
                    else {
                        LOGGER.error("Failed to create ticket, no ID obtained.");
                    }
                }
            }
        } catch (SQLException e) {
            LOGGER.error(e.getMessage());
        }

        return ticket;
    }

    @Override
    public Ticket updateTicket(Ticket ticket) {
        LOGGER.info("Updating ticket");

        try(Connection connection = connectionPool.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(updateQuery);
            statement.setLong(1, ticket.getUser().getId());
            statement.setLong(2, ticket.getExhibition().getId());
            statement.setBigDecimal(3, new BigDecimal(ticket.getPrice()));

            boolean result = statement.execute();

            if(result) {
                LOGGER.info("Ticket update failed");
            }
            else {
                LOGGER.info("Ticket updated successfully");
            }
        } catch (SQLException e) {
            LOGGER.error(e.getMessage());
        }

        return ticket;
    }

    @Override
    public void deleteTicketById(Long id) {
        LOGGER.info("Deleting ticket");

        try(Connection connection = connectionPool.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(deleteQuery);
            statement.setLong(1, id);

            boolean result = statement.execute();

            if(result) {
                LOGGER.info("Ticket deletion failed");
            }
            else {
                LOGGER.info("Ticket deleted successfully");
            }
        } catch (SQLException e) {
            LOGGER.error(e.getMessage());
        }
    }

    @Override
    public Ticket findTicketById(Long id, String locale) {
        LOGGER.info("Getting ticket with id " + id);
        Ticket ticket = new Ticket();

        try(Connection connection = connectionPool.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(findByIdQuery);
            statement.setLong(1, id);

            ResultSet result = statement.executeQuery();

            if(result.next()) {
                ticket.setId(result.getLong("id"));
                ticket.setUser(userDao.findUserById(result.getLong("user_id")));
                ticket.setExhibition(exhibitionDao.findExhibitionById(result.getLong("exhibition_id"), locale));
                ticket.setPrice(result.getBigDecimal("price").floatValue());
            }
        } catch (SQLException e) {
            LOGGER.error(e.getMessage());
        }

        return ticket;
    }

    @Override
    public boolean checkIfUserHasTicket(Long userId, Long exhibitionId) {
        LOGGER.info("Checking if user " + userId + " has ticket to exhibition " + exhibitionId);
        boolean result = false;

        try(Connection connection = connectionPool.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(checkIfUserHasTicketQuery);
            statement.setLong(1, userId);
            statement.setLong(2, exhibitionId);

            ResultSet res = statement.executeQuery();

            if(res.next()) {
                result = true;
            }

        } catch (SQLException e) {
            LOGGER.error(e.getMessage());
        }

        return result;
    }

    @Override
    public List<Ticket> findTicketsByUserId(Long userId, String locale) {
        LOGGER.info("Finding tickets for user with id " + userId);
        List<Ticket> res = new ArrayList<>();

        try(Connection connection = connectionPool.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(findByUserIdQuery);
            statement.setLong(1, userId);

            ResultSet result = statement.executeQuery();

            res = getTickets(result, locale);

        } catch (SQLException e) {
            LOGGER.error(e.getMessage());
        }

        return res;
    }

    @Override
    public List<Ticket> findPage(Integer offset, Integer size, String locale) {
        LOGGER.info("Getting page with offset " + offset + ", size " + size);
        List<Ticket> res = new ArrayList<>();

        try(Connection connection = connectionPool.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(findPageQuery);
            statement.setInt(1, offset);
            statement.setInt(2, size);

            ResultSet result = statement.executeQuery();

            res = getTickets(result, locale);

        } catch (SQLException e) {
            LOGGER.error(e.getMessage());
        }

        return res;
    }

    @Override
    public Integer numberOfSoldTickets(Long exhibitionId) {
        LOGGER.info("Getting quantity of sold tickets" );

        int count = 0;

        try(Connection connection = connectionPool.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(findSoldTickets);
            statement.setLong(1, exhibitionId);

            ResultSet result = statement.executeQuery();
            if(result.next()) {
                count = result.getInt("count");
            }


        } catch (SQLException e) {
            LOGGER.error(e.getMessage());
        }

        return count;
    }

    private List<Ticket> getTickets(ResultSet result, String locale) {
        List<Ticket> res = new ArrayList<>();

        try {
            while (result.next()) {
                Ticket subscription = new Ticket();
                subscription.setId(result.getLong("id"));
                subscription.setUser(userDao.findUserById(result.getLong("user_id")));
                subscription.setExhibition(exhibitionDao.findExhibitionById(result.getLong("exhibition_id"), locale));
                subscription.setPrice(result.getBigDecimal("price").floatValue());

                res.add(subscription);
            }
        } catch (SQLException e) {
            LOGGER.error(e.getMessage());
        }

        return res;
    }
}
