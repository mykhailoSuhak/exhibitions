package com.exhibitions.dao.ticket;

import com.exhibitions.model.Ticket;

import java.util.List;

public interface TicketDao {
    /**
     * This method creates new ticket.
     *
     * @param ticket Object to be created.
     * @return             Created object.
     */
    Ticket createTicket(Ticket ticket);

    /**
     * This method updated subscription.
     *
     * @param ticket Object to be updated.
     * @return             Updated object.
     */
    Ticket updateTicket(Ticket ticket);

    /**
     * This method deletes ticket by id.
     *
     * @param id Id of ticket to be deleted.
     */
    void deleteTicketById(Long id);

    /**
     * This method finds ticket by id.
     *
     * @param id Id of ticket to find.
     * @return   Object found by id.
     */
    Ticket findTicketById(Long id, String locale);

    /**
     * This method returns weather user has ticket to an exhibition.
     *
     * @param userId     Id of the user.
     * @param exhibitionId Id of the exhibition.
     * @return           True if user has ticket, otherwise false.
     */
    boolean checkIfUserHasTicket(Long userId, Long exhibitionId);

    /**
     * This method returns tickets of user.
     *
     * @param userId Id of the user to find tickets for.
     * @return       A list of user's tickets.
     */
    List<Ticket> findTicketsByUserId(Long userId, String locale);

    /**
     * This methods finds page from all tickets.
     *
     * @param offset Element to start from.
     * @param size   How much elements to take.
     * @return       List of tickets.
     */
    List<Ticket> findPage(Integer offset, Integer size, String locale);

    /**
     * This methods finds quantity of sold tickets to the current exhibition.
     *
     * @param exhibitionId Id of the exhibition.
     * @return       number of sold tickets.
     */
    Integer numberOfSoldTickets(Long exhibitionId);
}

