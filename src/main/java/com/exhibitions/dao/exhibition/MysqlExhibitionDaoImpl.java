package com.exhibitions.dao.exhibition;

import com.exhibitions.connection.ConnectionPool;
import com.exhibitions.dao.category.MysqlCategoryDaoImpl;
import com.exhibitions.dao.hall.MysqlHallDaoImpl;
import com.exhibitions.model.Exhibition;
import com.exhibitions.model.ExhibitionBuilder;
import com.exhibitions.properties.MysqlQueryProperties;
import org.apache.log4j.Logger;

import java.io.InputStream;
import java.math.BigDecimal;
import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class MysqlExhibitionDaoImpl implements ExhibitionDao {

    private static final Logger LOGGER = Logger.getLogger(MysqlExhibitionDaoImpl.class);

    private static MysqlExhibitionDaoImpl INSTANCE;
    private static ConnectionPool connectionPool;
    private MysqlCategoryDaoImpl categoryDao;
    private MysqlHallDaoImpl hallDao;

    private static String createQuery;
    private static String updateQuery;
    private static String deleteQuery;
    private static String findByIdQuery;
    private static String findLastQuery;
    private static String findPageByCategoryQuery;
    private static String findPageByHallQuery;
    private static String findPage;
    private static String findPageByName;
    private static String findPageByNameUkr;
    private static String findPageByTimeQuery;
    private static String findImageQuery;
    private static String findByPriceAsc;
    private static String findByPriceDesc;

    private MysqlExhibitionDaoImpl() {
        LOGGER.info("Initializing MysqlExhibitionDaoImpl");

        connectionPool = ConnectionPool.getInstance();
        MysqlQueryProperties properties = MysqlQueryProperties.getInstance();

        createQuery = properties.getProperty("createExhibition");
        updateQuery = properties.getProperty("updateExhibitionById");
        deleteQuery = properties.getProperty("deleteExhibitionById");
        findByIdQuery = properties.getProperty("findExhibitionById");
        findLastQuery = properties.getProperty("findLastExhibitions");
        findPageByCategoryQuery = properties.getProperty("findPageByCategory");
        findPage = properties.getProperty("findPage");
        findPageByName = properties.getProperty("findPageByNameSearch");
        findPageByNameUkr = properties.getProperty("findPageByNameUkrSearch");
        findPageByTimeQuery = properties.getProperty("findPageByTime");
        findImageQuery =  properties.getProperty("findImage");
        findByPriceAsc =  properties.getProperty("findPageByPriceAsc");
        findByPriceDesc =  properties.getProperty("findPageByPriceDesc");

        categoryDao = MysqlCategoryDaoImpl.getInstance();
        hallDao = MysqlHallDaoImpl.getInstance();
    }

    public static MysqlExhibitionDaoImpl getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new MysqlExhibitionDaoImpl();
        }
        return INSTANCE;
    }

    @Override
    public Exhibition createExhibition(Exhibition exhibition, InputStream image) {
        LOGGER.info("Creating new exhibition");

        try (Connection connection = connectionPool.getConnection()) {
            //connection.setAutoCommit(false);
            PreparedStatement statement = connection.prepareStatement(createQuery, Statement.RETURN_GENERATED_KEYS);
            statement.setString(1, exhibition.getTitle());
            statement.setString(2, exhibition.getTitle_ukr());
            statement.setBigDecimal(3, new BigDecimal(exhibition.getPrice()));
            statement.setLong(4, exhibition.getCategory().getId());
            statement.setString(5, exhibition.getDescription());
            statement.setString(6, exhibition.getDescription_ukr());
            statement.setDate(7, Date.valueOf(exhibition.getStartDate()));
            statement.setDate(8, Date.valueOf(exhibition.getEndDate()));
            statement.setBlob(9, image);

            int affectedRows = statement.executeUpdate();

            if (affectedRows == 0) {
                LOGGER.info("Exhibition creation failed");
            } else {
                LOGGER.info("Exhibition creation successful");

                try (ResultSet generatedKeys = statement.getGeneratedKeys()) {
                    if (generatedKeys.next()) {
                        exhibition.setId(generatedKeys.getLong(1));
                    } else {
                        LOGGER.error("Failed to create exhibition, no ID obtained.");
                    }
                }
            }
        } catch (SQLException e) {
            LOGGER.error(e.getMessage());
        }

        return exhibition;
    }

    @Override
    public Exhibition updateExhibition(Exhibition exhibition, InputStream image) {
        LOGGER.info("Updating exhibition");

        try (Connection connection = connectionPool.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(updateQuery);
            statement.setString(1, exhibition.getTitle());
            statement.setString(2, exhibition.getTitle_ukr());
            statement.setBigDecimal(3, new BigDecimal(exhibition.getPrice()));
            statement.setLong(4, exhibition.getCategory().getId());
            statement.setString(5, exhibition.getDescription());
            statement.setString(6, exhibition.getDescription_ukr());
            statement.setDate(7, Date.valueOf(exhibition.getStartDate()));
            statement.setDate(8, Date.valueOf(exhibition.getEndDate()));
            statement.setBlob(9, image);
            statement.setLong(10, exhibition.getId());

            boolean result = statement.execute();

            if (result) {
                LOGGER.info("Exhibition update failed");
            } else {
                LOGGER.info("Exhibition updated successfully");
            }
        } catch (SQLException e) {
            LOGGER.error(e.getMessage());
        }

        return exhibition;
    }

    @Override
    public boolean deleteExhibitionById(Long id) {
        LOGGER.info("Deleting exhibition");
        boolean res = false;

        try (Connection connection = connectionPool.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(deleteQuery);
            statement.setLong(1, id);

            boolean result = statement.execute();

            if (result) {
                LOGGER.info("Exhibition deletion failed");
            } else {
                LOGGER.info("Exhibition deleted successfully");
                res = true;
            }
        } catch (SQLException e) {
            LOGGER.error(e.getMessage());
        }

        return res;
    }

    @Override
    public Exhibition findExhibitionById(Long id, String locale) {
        LOGGER.info("Getting exhibition with id " + id);
        ExhibitionBuilder exhibitionBuilder = null;

        try (Connection connection = connectionPool.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(findByIdQuery);
            statement.setLong(1, id);

            ResultSet result = statement.executeQuery();

            if (result.next()) {
                exhibitionBuilder = new ExhibitionBuilder();
                exhibitionBuilder.setId(result.getLong("id"));
                if ("ru".equals(locale)) {
                    exhibitionBuilder.setTitle(result.getString("name_ukr"));
                    exhibitionBuilder.setDescription(result.getString("description_ukr"));
                } else {
                    exhibitionBuilder.setTitle(result.getString("name"));
                    exhibitionBuilder.setDescription(result.getString("description"));
                }
                exhibitionBuilder.setPrice(result.getBigDecimal("price").floatValue());
                exhibitionBuilder.setCategory(categoryDao.findCategoryById(result.getLong("category_id"), locale));
                exhibitionBuilder.setStartDate(result.getDate("startDate").toLocalDate());
                exhibitionBuilder.setEndDate(result.getDate("endDate").toLocalDate());
                exhibitionBuilder.setHalls(hallDao.findHallsByExhibitionId(result.getLong("id"), locale));

            }
        } catch (SQLException e) {
            LOGGER.error(e.getMessage());
        }

        return exhibitionBuilder.build();
    }

    @Override
    public List<Exhibition> findLastExhibitions(Integer limit, String locale) {
        LOGGER.info("Getting " + limit + " latest exhibitions");
        List<Exhibition> res = new ArrayList<>();

        try (Connection connection = connectionPool.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(findLastQuery);
            statement.setInt(1, limit);

            ResultSet result = statement.executeQuery();

            res = getExhibitions(result, locale);
        } catch (SQLException e) {
            LOGGER.error(e.getMessage());
        }

        return res;
    }

    @Override
    public List<Exhibition> findPageByCategory(Long categoryId, Integer offset, Integer size, String locale) {
        LOGGER.info("Getting page with offset " + offset + ", size " + size + " of category id " + categoryId);
        List<Exhibition> res = new ArrayList<>();

        try (Connection connection = connectionPool.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(findPageByCategoryQuery);
            statement.setLong(1, categoryId);
            statement.setInt(2, offset);
            statement.setInt(3, size);

            ResultSet result = statement.executeQuery();

            res = getExhibitions(result, locale);

        } catch (SQLException e) {
            LOGGER.error(e.getMessage());
        }

        return res;
    }

    @Override
    public List<Exhibition> findPageByTime(String startdate, String enddate, Integer offset, Integer size, String locale) {
        LOGGER.info("Getting page with offset " + offset + ", size " + size + " between dates " + startdate + " and " + enddate);
        List<Exhibition> res = new ArrayList<>();

        try (Connection connection = connectionPool.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(findPageByTimeQuery);
            statement.setString(1, enddate);
            statement.setString(2, startdate);
            statement.setInt(3, offset);
            statement.setInt(4, size);

            ResultSet result = statement.executeQuery();

            res = getExhibitions(result, locale);

        } catch (SQLException e) {
            LOGGER.error(e.getMessage());
        }

        return res;
    }

    @Override
    public List<Exhibition> findPageByPrice(Boolean diretion, Integer offset, Integer size, String locale) {
        LOGGER.info("Getting page with offset " + offset + ", size " + size );
        List<Exhibition> res = new ArrayList<>();

        String query = "";
        if (diretion) query = findByPriceAsc;
        else query = findByPriceDesc;

        try (Connection connection = connectionPool.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setInt(1, offset);
            statement.setInt(2, size);

            ResultSet result = statement.executeQuery();

            res = getExhibitions(result, locale);

        } catch (SQLException e) {
            LOGGER.error(e.getMessage());
        }

        return res;
    }


    @Override
    public List<Exhibition> findPage(Integer offset, Integer size, String locale) {
        LOGGER.info("Getting page with offset " + offset + ", size " + size);
        List<Exhibition> res = new ArrayList<>();

        try (Connection connection = connectionPool.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(findPage);
            statement.setInt(1, offset);
            statement.setInt(2, size+1);

            ResultSet result = statement.executeQuery();

            res = getExhibitions(result, locale);
            LOGGER.info(res.size());

        } catch (SQLException e) {
            LOGGER.error(e.getMessage());
        }

        return res;
    }

    @Override
    public List<Exhibition> findPageByNameQuery(String query, Integer offset, Integer size, String locale) {
        LOGGER.info("Getting page by query " + query + ", with offset " + offset + ", size " + size);

        List<Exhibition> res = new ArrayList<>();
        PreparedStatement statement = null;

        try (Connection connection = connectionPool.getConnection()) {
            if ("ru".equals(locale))
                statement = connection.prepareStatement(findPageByNameUkr);
            else
                statement = connection.prepareStatement(findPageByName);

            statement.setString(1, "%" + query + "%");
            statement.setInt(2, offset);
            statement.setInt(3, size);

            ResultSet result = statement.executeQuery();

            res = getExhibitions(result, locale);

        } catch (SQLException e) {
            LOGGER.error(e.getMessage());
        }

        return res;
    }

    private List<Exhibition> getExhibitions(ResultSet result, String locale) {
        List<Exhibition> res = new ArrayList<>();

        try {
            while (result.next()) {
                ExhibitionBuilder exhibitionBuilder = new ExhibitionBuilder();

                exhibitionBuilder.setId(result.getLong("id"));
                if ("ru".equals(locale)) {
                    exhibitionBuilder.setTitle(result.getString("name_ukr"));
                    exhibitionBuilder.setDescription(result.getString("description_ukr"));
                } else {
                    exhibitionBuilder.setTitle(result.getString("name"));
                    exhibitionBuilder.setDescription(result.getString("description"));
                }
                exhibitionBuilder.setPrice(result.getBigDecimal("price").floatValue());
                exhibitionBuilder.setCategory(categoryDao.findCategoryById(result.getLong("category_id"), locale));
                exhibitionBuilder.setStartDate(result.getDate("startDate").toLocalDate());
                exhibitionBuilder.setEndDate(result.getDate("endDate").toLocalDate());
                exhibitionBuilder.setHalls(hallDao.findHallsByExhibitionId(result.getLong("id"), locale));

                res.add(exhibitionBuilder.build());
            }
        } catch (SQLException e) {
            LOGGER.error(e.getMessage());
        }

        return res;
    }

    @Override
    public byte[] findImageById(Long id) {
        LOGGER.info("Finding image by id " + id);
        byte[] result = new byte[0];

        try(Connection connection = connectionPool.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(findImageQuery);
            statement.setLong(1, id);

            ResultSet res = statement.executeQuery();

            if(res.next()) {
                result = res.getBytes("image");
            }

        } catch (SQLException e) {
            LOGGER.error(e.getMessage());
        }

        return result;
    }

    @Override
    public Long createImage(InputStream inputStream) {
        LOGGER.info("Creating image");
        Long id = null;

        try(Connection connection = connectionPool.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(createQuery, Statement.RETURN_GENERATED_KEYS);
            statement.setBlob(1, inputStream);

            int affectedRows = statement.executeUpdate();

            if(affectedRows == 0) {
                LOGGER.info("Image creation failed");
            }
            else {
                LOGGER.info("Image creation successful");

                try (ResultSet generatedKeys = statement.getGeneratedKeys()) {
                    if (generatedKeys.next()) {
                        id = generatedKeys.getLong(1);
                    }
                    else {
                        LOGGER.error("No ID obtained.");
                    }
                }
            }
        } catch (SQLException e) {
            LOGGER.error(e.getMessage());
        }

        return id;
    }

    @Override
    public boolean deleteImageById(Long id) {
        LOGGER.info("Deleting image by id " + id);
        boolean result = false;

        try(Connection connection = connectionPool.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(deleteQuery);
            statement.setLong(1, id);

            boolean res = statement.execute();

            if(res) {
                LOGGER.info("Failed to delete image by id " + id);
            }
            else {
                LOGGER.info("Image deleted successfully");
                result = true;
            }
        } catch (SQLException e) {
            LOGGER.error(e.getMessage());
        }

        return result;
    }
}

