package com.exhibitions.dao.exhibition;

import com.exhibitions.model.Exhibition;

import java.io.InputStream;
import java.util.List;

public interface ExhibitionDao {
    /**
     * This method creates new exhibition.
     *
     * @param exhibition Object to be created.
     * @return An updated object.
     */
    Exhibition createExhibition(Exhibition exhibition, InputStream image);

    /**
     * This method updates exhibition.
     *
     * @param exhibition Object to be updated in db.
     * @return An updated object.
     */
    Exhibition updateExhibition(Exhibition exhibition, InputStream image);

    /**
     * This method deletes exhibition by id.
     *
     * @param id Id of the exhibition to be deleted.
     * @return True if deletion successful, otherwise false.
     */
    boolean deleteExhibitionById(Long id);

    /**
     * This method finds exhibition by id.
     *
     * @param id Id of the exhibition to find.
     * @return Exhibition object.
     */
    Exhibition findExhibitionById(Long id, String locale);

    /**
     * This method finds latest added exhibitions.
     *
     * @param limit How much to find.
     * @return A list of exhibitions.
     */
    List<Exhibition> findLastExhibitions(Integer limit, String locale);

    /**
     * This method finds a page of exhibitions by category.
     *
     * @param categoryId Id of the category of exhibitions to find
     * @param offset     Element to start from.
     * @param size       How much elements to take.
     * @return List of exhibitions.
     */
    List<Exhibition> findPageByCategory(Long categoryId, Integer offset, Integer size, String locale);

    /**
     * This method finds a page of exhibitions by time.
     *
     * @param startdate Start Date of exhibitions to find
     * @param enddate   End Date of exhibitions to find
     * @param offset    Element to start from.
     * @param size      How much elements to take.
     * @return List of exhibitions.
     */
    List<Exhibition> findPageByTime(String startdate, String enddate, Integer offset, Integer size, String locale);

    /**
     * This method finds a page of exhibitions by price.
     *
     * @param direction If true, cheap price first, false - cheap price last
     * @param offset    Element to start from.
     * @param size      How much elements to take.
     * @return List of exhibitions.
     */
    List<Exhibition> findPageByPrice(Boolean direction, Integer offset, Integer size, String locale);


    /**
     * This methods finds page from all exhibitions
     *
     * @param offset Element to start from.
     * @param size   How much elements to take.
     * @return List of exhibitions.
     */
    List<Exhibition> findPage(Integer offset, Integer size, String locale);

    /**
     * This methods finds page from all exhibitions by name.
     *
     * @param query  Search query.
     * @param offset Element to start from.
     * @param size   How much elements to take.
     * @return List of exhibitions.
     */
    List<Exhibition> findPageByNameQuery(String query, Integer offset, Integer size, String locale);

    /**
     * This method finds image by id.
     *
     * @param id Id of the image to find.
     * @return   Image bytes.
     */
    byte[] findImageById(Long id);

    /**
     * This method creates new image.
     *
     * @param inputStream Image stream.
     * @return            Id of inserted image.
     */
    Long createImage(InputStream inputStream);

    /**
     * This method deletes image by id.
     *
     * @param id Id of the image to delete.
     * @return   True if deletion was successful, otherwise false.
     */
    boolean deleteImageById(Long id);
}

