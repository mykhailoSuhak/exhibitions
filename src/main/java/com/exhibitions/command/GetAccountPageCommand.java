package com.exhibitions.command;

import com.exhibitions.dao.ticket.MysqlTicketDaoImpl;
import com.exhibitions.dao.user.MysqlUserDaoImpl;
import com.exhibitions.model.Ticket;
import com.exhibitions.model.User;
import com.exhibitions.properties.MappingProperties;
import com.exhibitions.service.ticket.TicketService;
import com.exhibitions.service.ticket.TicketServiceImpl;
import com.exhibitions.service.user.UserService;
import com.exhibitions.service.user.UserServiceImpl;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * This class is used to handle GET requests to the user account page.
 */
public class GetAccountPageCommand implements ServletCommand {

    private static final Logger LOGGER = Logger.getLogger(GetLoginPageCommand.class);

    private static UserService userService;
    private static TicketService ticketService;

    private static String mainPage;
    private static String accountPage;

    public GetAccountPageCommand(){
        LOGGER.info("Initializing GetAccountPageCommand");

        userService = new UserServiceImpl(MysqlUserDaoImpl.getInstance());
        ticketService = new TicketServiceImpl(MysqlTicketDaoImpl.getInstance());

        MappingProperties properties = MappingProperties.getInstance();
        mainPage = properties.getProperty("mainPage");
        accountPage = properties.getProperty("accountPage");
    }

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response, String locale) {
        LOGGER.info("Executing GetAccountPageCommand");
        String resultPage = mainPage;

        if(request.getSession().getAttribute("authenticated") != null &&
                request.getSession().getAttribute("authenticated").equals(true)) {

            User user = userService.findUserByEmail(request.getSession().getAttribute("email").toString());
            List<Ticket> tickets = ticketService.getUserTickets(user.getId(), locale);

            request.setAttribute("user", user);
            request.setAttribute("noTickets", tickets.isEmpty());
            request.setAttribute("tickets", tickets);

            resultPage = accountPage;
        }

        return resultPage;
    }
}

