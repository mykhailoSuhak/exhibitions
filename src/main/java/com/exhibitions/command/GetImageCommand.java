package com.exhibitions.command;


import com.exhibitions.dao.exhibition.MysqlExhibitionDaoImpl;
import com.exhibitions.properties.MappingProperties;
import com.exhibitions.service.exhibition.ExhibitionService;
import com.exhibitions.service.exhibition.ExhibitionServiceImpl;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * This class is used to handle GET requests to retrieve images.
 */
public class GetImageCommand implements ServletCommand {

    private static final Logger LOGGER = Logger.getLogger(GetImageCommand.class);

    private static ExhibitionService exhibitionService;

    private static String errorpage;

    public GetImageCommand(){
        LOGGER.info("Initializing GetImageCommand");

        exhibitionService = new ExhibitionServiceImpl(MysqlExhibitionDaoImpl.getInstance());

        MappingProperties properties = MappingProperties.getInstance();
        errorpage = properties.getProperty("error404Page");
    }

    public String execute(HttpServletRequest request, HttpServletResponse response, String locale) {
        LOGGER.info("Executing GetImageCommand");

        String resultPage = errorpage;

        if(request.getParameter("id") != null) {
            try {
                Long id = Long.parseLong(request.getParameter("id"));

                byte[] image =  exhibitionService.findImageById(id);

                if(image != null && image.length != 0) {
                    response.setContentType("image/png");
                    response.setContentLength(image.length);
                    response.getOutputStream().write(image);
                }
            }
            catch (NumberFormatException ex) {
                LOGGER.info("Couldn't parse " + request.getParameter("id") + " to long");
            } catch (IOException e) {
                LOGGER.info(e.getMessage());
            }

        }

        return resultPage;
    }
}
