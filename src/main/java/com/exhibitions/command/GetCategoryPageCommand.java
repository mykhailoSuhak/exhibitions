package com.exhibitions.command;

import com.exhibitions.dao.category.MysqlCategoryDaoImpl;
import com.exhibitions.dao.exhibition.MysqlExhibitionDaoImpl;
import com.exhibitions.model.Category;
import com.exhibitions.model.Exhibition;
import com.exhibitions.properties.MappingProperties;
import com.exhibitions.service.category.CategoryService;
import com.exhibitions.service.category.CategoryServiceImpl;
import com.exhibitions.service.exhibition.ExhibitionService;
import com.exhibitions.service.exhibition.ExhibitionServiceImpl;
import com.exhibitions.util.Page;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * This class is used to handle GET requests to the page to view all the exhibitions
 * of the particular category.
 */
public class GetCategoryPageCommand implements ServletCommand {

    private static final Logger LOGGER = Logger.getLogger(GetCategoryPageCommand.class);

    private static CategoryService categoryService;
    private static ExhibitionService exhibitionService;

    private static String categoryPage;
    private static String errorPage;

    public GetCategoryPageCommand(){
        LOGGER.info("Initializing GetCategoryPageCommand");

        categoryService = new CategoryServiceImpl(MysqlCategoryDaoImpl.getInstance());
        exhibitionService = new ExhibitionServiceImpl(MysqlExhibitionDaoImpl.getInstance());

        MappingProperties properties = MappingProperties.getInstance();
        categoryPage = properties.getProperty("categoryPage");
        errorPage = properties.getProperty("error404Page");
    }

    public String execute(HttpServletRequest request, HttpServletResponse response, String locale) {
        LOGGER.info("Executing command");

        String resultPage = errorPage;
        HttpSession session = request.getSession();

        if(request.getParameter("catId") != null && request.getParameter("p") != null &&
                request.getParameter("s") != null) {

            try {
                Long categoryId = Long.parseLong(request.getParameter("catId"));
                Integer pageNum = Integer.parseInt(request.getParameter("p"));
                Integer size = Integer.parseInt(request.getParameter("s"));

                Category category = categoryService.findCategoryById(categoryId, locale);
                Page<Exhibition> page = exhibitionService.getPageByCategoryId(pageNum, size, category.getId(), locale);

                request.setAttribute("categories", categoryService.findAll(locale));
                request.setAttribute("page", page);
                request.setAttribute("category", category);

                resultPage = categoryPage;
            }
            catch (NumberFormatException ex) {
                LOGGER.info("Couldn't parse " + request.getParameter("catId") + ", "
                        + request.getParameter("p") + ", "
                        + request.getParameter("s")+ " to long");
            }
        }

        return resultPage;
    }
}
