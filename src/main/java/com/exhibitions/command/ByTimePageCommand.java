package com.exhibitions.command;

import com.exhibitions.dao.category.MysqlCategoryDaoImpl;
import com.exhibitions.dao.exhibition.MysqlExhibitionDaoImpl;
import com.exhibitions.model.Category;
import com.exhibitions.model.Exhibition;
import com.exhibitions.properties.MappingProperties;
import com.exhibitions.service.category.CategoryService;
import com.exhibitions.service.category.CategoryServiceImpl;
import com.exhibitions.service.exhibition.ExhibitionService;
import com.exhibitions.service.exhibition.ExhibitionServiceImpl;
import com.exhibitions.util.Page;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * This class is used to handle GET requests to the page to view all the exhibitions
 * by time, set by user.
 */
public class ByTimePageCommand implements ServletCommand {

    private static final Logger LOGGER = Logger.getLogger(GetCategoryPageCommand.class);

    private static CategoryService categoryService;
    private static ExhibitionService exhibitionService;

    private static String byTimePage;
    private static String errorPage;

    public ByTimePageCommand() {
        LOGGER.info("Initializing ByTimePageCommand");

        categoryService = new CategoryServiceImpl(MysqlCategoryDaoImpl.getInstance());
        exhibitionService = new ExhibitionServiceImpl(MysqlExhibitionDaoImpl.getInstance());

        MappingProperties properties = MappingProperties.getInstance();
        byTimePage = properties.getProperty("byTimePage");
        errorPage = properties.getProperty("error404Page");
    }

    public String execute(HttpServletRequest request, HttpServletResponse response, String locale) {
        LOGGER.info("Executing byTimePageCommand");

        String resultPage = errorPage;
        HttpSession session = request.getSession();

        if (request.getParameter("startdate") != null && request.getParameter("enddate") != null && request.getParameter("p") != null &&
                request.getParameter("s") != null) {

            try {
                String startdate = request.getParameter("startdate");
                String enddate = request.getParameter("enddate");

                Integer pageNum = Integer.parseInt(request.getParameter("p"));
                Integer size = Integer.parseInt(request.getParameter("s"));

                Page<Exhibition> page = exhibitionService.getPageByTime(pageNum, size, startdate, enddate, locale);

                request.setAttribute("categories", categoryService.findAll(locale));

                request.setAttribute("page", page);
                request.setAttribute("startDate", startdate);
                request.setAttribute("endDate", enddate);

                resultPage = byTimePage;
            } catch (NumberFormatException ex) {
                LOGGER.info("Couldn't parse " + request.getParameter("catId") + ", "
                        + request.getParameter("p") + ", "
                        + request.getParameter("s") + " to long");
            }
        }

        return resultPage;
    }
}
