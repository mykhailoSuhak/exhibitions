package com.exhibitions.command;

import com.exhibitions.dao.category.MysqlCategoryDaoImpl;
import com.exhibitions.dao.exhibition.MysqlExhibitionDaoImpl;
import com.exhibitions.model.Exhibition;
import com.exhibitions.properties.MappingProperties;
import com.exhibitions.service.category.CategoryService;
import com.exhibitions.service.category.CategoryServiceImpl;
import com.exhibitions.service.exhibition.ExhibitionService;
import com.exhibitions.service.exhibition.ExhibitionServiceImpl;
import com.exhibitions.util.Page;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class GetSearchPageCommand implements ServletCommand {

    private static final Logger LOGGER = Logger.getLogger(GetSearchPageCommand.class);

    private static CategoryService categoryService;
    private static ExhibitionService exhibitionService;

    private static String exhibitionsPage;
    private static String errorPage;

    public GetSearchPageCommand(){
        LOGGER.info("Initializing GetSearchPageCommand");

        categoryService = new CategoryServiceImpl(MysqlCategoryDaoImpl.getInstance());
        exhibitionService = new ExhibitionServiceImpl(MysqlExhibitionDaoImpl.getInstance());

        MappingProperties properties = MappingProperties.getInstance();
        exhibitionsPage = properties.getProperty("searchExhibitionsPage");
        errorPage = properties.getProperty("error404Page");
    }

    public String execute(HttpServletRequest request, HttpServletResponse response, String locale) {
        LOGGER.info("Executing GetSearchPageCommand");

        String resultPage = errorPage;

        if(request.getParameter("q") != null && request.getParameter("p") != null &&
                request.getParameter("s") != null) {

            try {
                String query = request.getParameter("q");
                Integer pageNum = Integer.parseInt(request.getParameter("p"));
                Integer size = Integer.parseInt(request.getParameter("s"));

                Page<Exhibition> page = exhibitionService.getPageByName(query, pageNum, size, locale);

                request.setAttribute("categories", categoryService.findAll(locale));
                request.setAttribute("page", page);
                request.setAttribute("query", query);

                resultPage = exhibitionsPage;
            }
            catch (NumberFormatException ex) {
                LOGGER.info("Couldn't parse " + request.getParameter("catId") + ", "
                        + request.getParameter("p") + ", "
                        + request.getParameter("s")+ " to long");
            }
        }

        return resultPage;
    }
}