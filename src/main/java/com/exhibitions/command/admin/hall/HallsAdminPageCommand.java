package com.exhibitions.command.admin.hall;

import com.exhibitions.command.ServletCommand;
import com.exhibitions.dao.hall.MysqlHallDaoImpl;
import com.exhibitions.model.UserType;
import com.exhibitions.properties.MappingProperties;
import com.exhibitions.service.hall.HallService;
import com.exhibitions.service.hall.HallServiceImpl;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * This class is used to handle GET requests to the admin page to view all publishers.
 */
public class HallsAdminPageCommand implements ServletCommand {
    private static final Logger LOGGER = Logger.getLogger(HallsAdminPageCommand.class);

    private static HallService hallService;

    private static String page;
    private static String loginPage;

    public HallsAdminPageCommand() {
        LOGGER.info("Initializing HallsAdminPageCommand");

        hallService = new HallServiceImpl(MysqlHallDaoImpl.getInstance());

        MappingProperties properties = MappingProperties.getInstance();
        page = properties.getProperty("adminHallsPage");
        loginPage = properties.getProperty("loginPage");
    }

    public String execute(HttpServletRequest request, HttpServletResponse response, String locale) {
        LOGGER.info("Executing HallsAdminPageCommand");
        String resultPage = page;

        if (request.getSession().getAttribute("authenticated") == null ||
                request.getSession().getAttribute("authenticated").equals(false) ||
                !request.getSession().getAttribute("role").equals(UserType.ADMIN.name())) {
            LOGGER.info("User not authorized");
            resultPage = loginPage;
        } else
            request.setAttribute("halls", hallService.findAll(locale));

        return resultPage;
    }
}

