package com.exhibitions.command.admin.hall;

import com.exhibitions.command.ServletCommand;
import com.exhibitions.dao.hall.MysqlHallDaoImpl;
import com.exhibitions.model.Hall;
import com.exhibitions.model.UserType;
import com.exhibitions.properties.MappingProperties;
import com.exhibitions.service.hall.HallService;
import com.exhibitions.service.hall.HallServiceImpl;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * This page is used handle GET requests to the page used to edit publishers.
 */
public class EditHallAdminPageCommand implements ServletCommand {
    private static final Logger LOGGER = Logger.getLogger(EditHallAdminPageCommand.class);

    private static HallService hallService;

    private static String editHallPage;
    private static String hallsPage;
    private static String loginPage;

    public EditHallAdminPageCommand(){
        LOGGER.info("Initializing EditHallAdminPageCommand");

        hallService = new HallServiceImpl(MysqlHallDaoImpl.getInstance());

        MappingProperties properties = MappingProperties.getInstance();
        editHallPage = properties.getProperty("adminEditHallPage");
        hallsPage = properties.getProperty("adminHallsPage");
        loginPage = properties.getProperty("loginPage");
    }

    public String execute(HttpServletRequest request, HttpServletResponse response, String locale) {
        LOGGER.info("Executing EditHallAdminPageCommand");
        String resultPage = editHallPage;

        if (request.getSession().getAttribute("authenticated") == null ||
                request.getSession().getAttribute("authenticated").equals(false) ||
                !request.getSession().getAttribute("role").equals(UserType.ADMIN.name())) {
            LOGGER.info("User not authorized");
            resultPage = loginPage;
        }
        else if(request.getParameter("id") != null) {
            try {
                Long id = Long.parseLong(request.getParameter("id"));

                Hall hall = hallService.findHallById(id);

                if(hall != null) {
                    request.setAttribute("hall", hall);
                }
                else {
                    resultPage = hallsPage;
                }
            }
            catch (NumberFormatException ex) {
                LOGGER.info("Couldn't parse " + request.getParameter("id") + " to long");
            }

        }

        return resultPage;
    }
}
