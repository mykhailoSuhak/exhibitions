package com.exhibitions.command.admin.hall;

import com.exhibitions.command.ServletCommand;
import com.exhibitions.dao.hall.MysqlHallDaoImpl;
import com.exhibitions.model.Hall;
import com.exhibitions.model.UserType;
import com.exhibitions.properties.MappingProperties;
import com.exhibitions.service.hall.HallService;
import com.exhibitions.service.hall.HallServiceImpl;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * This class is used to handle POST requests to update publishers.
 */
public class UpdateHallAdminCommand implements ServletCommand {
    private static final Logger LOGGER = Logger.getLogger(UpdateHallAdminCommand.class);

    private static HallService hallService;

    private static String publishersPage;
    private static String loginPage;

    public UpdateHallAdminCommand(){
        LOGGER.info("Initializing UpdateHallAdminCommand");

        hallService = new HallServiceImpl(MysqlHallDaoImpl.getInstance());

        MappingProperties properties = MappingProperties.getInstance();
        publishersPage = properties.getProperty("adminHallsPage");
        loginPage = properties.getProperty("loginPage");
    }

    public String execute(HttpServletRequest request, HttpServletResponse response, String locale) {
        LOGGER.info("Executing UpdateHallAdminCommand");
        String resultPage = publishersPage;

        if (request.getSession().getAttribute("authenticated") == null ||
                request.getSession().getAttribute("authenticated").equals(false) ||
                !request.getSession().getAttribute("role").equals(UserType.ADMIN.name())) {
            LOGGER.info("User not authorized");
            resultPage = loginPage;
        }
        else if(request.getParameter("id") != null && request.getParameter("title") != null) {
            try {
                Long id = Long.parseLong(request.getParameter("id"));

                Hall hall = new Hall();
                hall.setId(id);
                hall.setTitle(request.getParameter("title"));
                hall.setTitle_ukr(request.getParameter("titleUkr"));
                hallService.updateHall(hall);

                request.setAttribute("updateSuccess", true);
                request.setAttribute("halls", hallService.findAll(locale));
            }
            catch (NumberFormatException ex) {
                LOGGER.info("Couldn't parse " + request.getParameter("id") + " to long");
            }

        }

        return resultPage;
    }
}

