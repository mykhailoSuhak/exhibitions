package com.exhibitions.command.admin.hall;

import com.exhibitions.command.ServletCommand;
import com.exhibitions.dao.hall.MysqlHallDaoImpl;
import com.exhibitions.model.UserType;
import com.exhibitions.properties.MappingProperties;
import com.exhibitions.service.hall.HallService;
import com.exhibitions.service.hall.HallServiceImpl;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * This class is used to handle GET requests to the page to add new publisher.
 */
public class AddHallAdminPageCommand implements ServletCommand {
    private static final Logger LOGGER = Logger.getLogger(AddHallAdminPageCommand.class);

    private static HallService hallService;

    private static String addHallPage;
    private static String loginPage;

    public AddHallAdminPageCommand(){
        LOGGER.info("Initializing AddHallAdminPageCommand");

        hallService = new HallServiceImpl(MysqlHallDaoImpl.getInstance());

        MappingProperties properties = MappingProperties.getInstance();
        addHallPage = properties.getProperty("adminAddHallPage");
        loginPage = properties.getProperty("loginPage");
    }

    public String execute(HttpServletRequest request, HttpServletResponse response, String locale) {
        LOGGER.info("Executing AddHallAdminPageCommand");
        String resultPage = addHallPage;

        if (request.getSession().getAttribute("authenticated") == null ||
                request.getSession().getAttribute("authenticated").equals(false) ||
                !request.getSession().getAttribute("role").equals(UserType.ADMIN.name())) {
            LOGGER.info("User not authorized");
            resultPage = loginPage;
        }

        return resultPage;
    }
}
