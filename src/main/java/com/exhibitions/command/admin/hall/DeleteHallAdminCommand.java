package com.exhibitions.command.admin.hall;

import com.exhibitions.command.ServletCommand;
import com.exhibitions.dao.hall.MysqlHallDaoImpl;
import com.exhibitions.model.UserType;
import com.exhibitions.properties.MappingProperties;
import com.exhibitions.service.hall.HallService;
import com.exhibitions.service.hall.HallServiceImpl;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * This class is used to handle GET requests to delete publisher.
 */
public class DeleteHallAdminCommand implements ServletCommand{
    private static final Logger LOGGER = Logger.getLogger(DeleteHallAdminCommand.class);

    private static HallService hallService;

    private static String hallsPage;
    private static String loginPage;

    public DeleteHallAdminCommand(){
        LOGGER.info("Initializing DeleteHallAdminCommand");

        hallService = new HallServiceImpl(MysqlHallDaoImpl.getInstance());

        MappingProperties properties = MappingProperties.getInstance();
        hallsPage = properties.getProperty("adminHallsPage");
        loginPage = properties.getProperty("loginPage");
    }

    public String execute(HttpServletRequest request, HttpServletResponse response, String locale) {
        LOGGER.info("Executing DeleteHallAdminCommand");
        String resultPage = hallsPage;

        if (request.getSession().getAttribute("authenticated") == null ||
                request.getSession().getAttribute("authenticated").equals(false) ||
                !request.getSession().getAttribute("role").equals(UserType.ADMIN.name())) {
            LOGGER.info("User not authorized");
            resultPage = loginPage;
        }
        else if(request.getParameter("id") != null) {
            try {
                Long id = Long.parseLong(request.getParameter("id"));

                request.setAttribute("deletionSuccess", hallService.deleteHallById(id));
                request.setAttribute("halls", hallService.findAll(locale));
            }
            catch (NumberFormatException ex) {
                LOGGER.info("Couldn't parse " + request.getParameter("id") + " to long");
            }
        }

        return resultPage;
    }
}
