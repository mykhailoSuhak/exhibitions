package com.exhibitions.command.admin.hall;

import com.exhibitions.command.ServletCommand;
import com.exhibitions.dao.hall.MysqlHallDaoImpl;
import com.exhibitions.model.Hall;
import com.exhibitions.model.UserType;
import com.exhibitions.properties.MappingProperties;
import com.exhibitions.service.hall.HallService;
import com.exhibitions.service.hall.HallServiceImpl;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * This class is used to handle POST requests to create new publisher.
 */
public class AddHallAdminCommand implements ServletCommand {
    private static final Logger LOGGER = Logger.getLogger(AddHallAdminCommand.class);

    private static HallService hallService;

    private static String addHallPage;
    private static String loginPage;

    public AddHallAdminCommand() {
        LOGGER.info("Initializing AddHallAdminCommand");

        hallService = new HallServiceImpl(MysqlHallDaoImpl.getInstance());

        MappingProperties properties = MappingProperties.getInstance();
        addHallPage = properties.getProperty("adminAddHallPage");
        loginPage = properties.getProperty("loginPage");
    }

    public String execute(HttpServletRequest request, HttpServletResponse response, String locale) {
        LOGGER.info("Executing AddHallAdminCommand");
        String resultPage = addHallPage;
        if (request.getSession().getAttribute("authenticated") == null ||
                request.getSession().getAttribute("authenticated").equals(false) ||
                !request.getSession().getAttribute("role").equals(UserType.ADMIN.name())) {
            LOGGER.info("User not authorized");
            resultPage = loginPage;
        } else if (request.getParameter("title") != null) {
            Hall hall = new Hall();
            hall.setTitle(request.getParameter("title"));
            hall.setTitle_ukr(request.getParameter("titleUkr"));

            hall = hallService.createHall(hall);

            request.setAttribute("success", hall != null);
        }

        return resultPage;
    }
}
