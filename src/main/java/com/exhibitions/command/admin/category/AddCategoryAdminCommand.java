package com.exhibitions.command.admin.category;

import com.exhibitions.command.ServletCommand;
import com.exhibitions.dao.category.MysqlCategoryDaoImpl;
import com.exhibitions.model.Category;
import com.exhibitions.model.UserType;
import com.exhibitions.properties.MappingProperties;
import com.exhibitions.service.category.CategoryService;
import com.exhibitions.service.category.CategoryServiceImpl;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * This class is used to handle POST requests to add new category.
 */
public class AddCategoryAdminCommand implements ServletCommand {
    private static final Logger LOGGER = Logger.getLogger(AddCategoryAdminCommand.class);

    private static CategoryService categoryService;

    private static String addCategoryPage;
    private static String loginPage;

    public AddCategoryAdminCommand(){
        LOGGER.info("Initializing AddCategoryAdminCommand");

        categoryService = new CategoryServiceImpl(MysqlCategoryDaoImpl.getInstance());

        MappingProperties properties = MappingProperties.getInstance();
        addCategoryPage = properties.getProperty("adminAddCategoryPage");
        loginPage = properties.getProperty("loginPage");
    }

    public String execute(HttpServletRequest request, HttpServletResponse response, String locale) {
        LOGGER.info("Executing command");
        String resultPage = addCategoryPage;

        if(request.getSession().getAttribute("authenticated") == null ||
                request.getSession().getAttribute("authenticated").equals(false) ||
                !request.getSession().getAttribute("role").equals(UserType.ADMIN.name())) {
            LOGGER.info("User not authorized");
            resultPage = loginPage;
        }
        else
        if(request.getParameter("name") != null && request.getParameter("nameUkr") != null) {
            Category category = new Category();
            category.setName(request.getParameter("name"));
            category.setName_ukr(request.getParameter("nameUkr"));

            category = categoryService.createCategory(category);

            request.setAttribute("success", category != null);
        }

        return resultPage;
    }
}
