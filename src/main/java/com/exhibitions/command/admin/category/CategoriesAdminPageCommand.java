package com.exhibitions.command.admin.category;

import com.exhibitions.command.ServletCommand;
import com.exhibitions.dao.category.MysqlCategoryDaoImpl;
import com.exhibitions.model.UserType;
import com.exhibitions.properties.MappingProperties;
import com.exhibitions.service.category.CategoryService;
import com.exhibitions.service.category.CategoryServiceImpl;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * This class is used to handle GET requests to the admin page used to view all categories.
 */
public class CategoriesAdminPageCommand implements ServletCommand {
    private static final Logger LOGGER = Logger.getLogger(CategoriesAdminPageCommand.class);

    private static CategoryService categoryService;

    private static String categoriesPage;
    private static String loginPage;

    public CategoriesAdminPageCommand(){
        LOGGER.info("Initializing CategoriesAdminPageCommand");

        categoryService = new CategoryServiceImpl(MysqlCategoryDaoImpl.getInstance());

        MappingProperties properties = MappingProperties.getInstance();
        categoriesPage = properties.getProperty("adminCategoriesPage");
        loginPage = properties.getProperty("loginPage");
    }

    public String execute(HttpServletRequest request, HttpServletResponse response, String locale) {
        LOGGER.info("Executing CategoriesAdminPageCommand");
        String resultPage = categoriesPage;
        if(request.getSession().getAttribute("authenticated") == null ||
                request.getSession().getAttribute("authenticated").equals(false) ||
                !request.getSession().getAttribute("role").equals(UserType.ADMIN.name())) {
            LOGGER.info("User not authorized");
            resultPage = loginPage;
        }
        else
        request.setAttribute("categories", categoryService.findAll(locale));

        return resultPage;
    }
}
