package com.exhibitions.command.admin.exhibition;

import com.exhibitions.command.ServletCommand;
import com.exhibitions.dao.category.MysqlCategoryDaoImpl;
import com.exhibitions.dao.hall.MysqlHallDaoImpl;
import com.exhibitions.model.UserType;
import com.exhibitions.properties.MappingProperties;
import com.exhibitions.service.category.CategoryService;
import com.exhibitions.service.category.CategoryServiceImpl;
import com.exhibitions.service.hall.HallService;
import com.exhibitions.service.hall.HallServiceImpl;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * This class is used to handle GET requests to the admin page used to add new magazine.
 */
public class GetAddExhibitionAdminPageCommand implements ServletCommand {

    private static final Logger LOGGER = Logger.getLogger(GetAddExhibitionAdminPageCommand.class);

    private static HallService hallService;
    private static CategoryService categoryService;

    private static String addExhibitionPage;
    private static String loginPage;

    public GetAddExhibitionAdminPageCommand(){
        LOGGER.info("Initializing GetAddExhibitionAdminPageCommand");

        hallService = new HallServiceImpl(MysqlHallDaoImpl.getInstance());
        categoryService = new CategoryServiceImpl(MysqlCategoryDaoImpl.getInstance());

        MappingProperties properties = MappingProperties.getInstance();
        addExhibitionPage = properties.getProperty("adminAddExhibitionPage");
        loginPage = properties.getProperty("loginPage");
    }

    public String execute(HttpServletRequest request, HttpServletResponse response, String locale) {
        LOGGER.info("Executing GetAddExhibitionAdminPageCommand");
        String resultPage = addExhibitionPage;

        if (request.getSession().getAttribute("authenticated") == null ||
                request.getSession().getAttribute("authenticated").equals(false) ||
                !request.getSession().getAttribute("role").equals(UserType.ADMIN.name())) {
            LOGGER.info("User not authorized");
            resultPage = loginPage;
        }
        else {
            request.setAttribute("halls", hallService.findAll(locale));
            request.setAttribute("categories", categoryService.findAll(locale));
        }

        return resultPage;
    }
}
