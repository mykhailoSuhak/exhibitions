package com.exhibitions.command.admin.exhibition;

import com.exhibitions.command.ServletCommand;
import com.exhibitions.dao.exhibition.MysqlExhibitionDaoImpl;
import com.exhibitions.model.Exhibition;
import com.exhibitions.model.UserType;
import com.exhibitions.properties.MappingProperties;
import com.exhibitions.service.exhibition.ExhibitionService;
import com.exhibitions.service.exhibition.ExhibitionServiceImpl;
import com.exhibitions.util.Page;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * This class is used to handle GET requests to the admin page used to display magazines.
 */
public class ExhibitionsAdminPageCommand implements ServletCommand {
    private static final Logger LOGGER = Logger.getLogger(ExhibitionsAdminPageCommand.class);

    private static ExhibitionService exhibitionService;

    private static String page;
    private static String loginPage;

    public ExhibitionsAdminPageCommand(){
        LOGGER.info("Initializing ExhibitionsAdminPageCommand");

        exhibitionService = new ExhibitionServiceImpl(MysqlExhibitionDaoImpl.getInstance());

        MappingProperties properties = MappingProperties.getInstance();
        page = properties.getProperty("adminExhibitionsPage");
        loginPage = properties.getProperty("loginPage");
    }

    public String execute(HttpServletRequest request, HttpServletResponse response, String locale) {
        LOGGER.info("Executing ExhibitionsAdminPageCommand");
        String resultPage = page;
        if (request.getSession().getAttribute("authenticated") == null ||
                request.getSession().getAttribute("authenticated").equals(false) ||
                !request.getSession().getAttribute("role").equals(UserType.ADMIN.name())) {
            LOGGER.info("User not authorized");
            resultPage = loginPage;
        } else
        try {
            Integer pageNum = Integer.parseInt(request.getParameter("p"));
            Integer size = Integer.parseInt(request.getParameter("s"));

            Page<Exhibition> page = exhibitionService.getPage(pageNum, size, locale);
                LOGGER.info("ExhibitionsAdminPage showing if page is last      " + page.isLast());
            request.setAttribute("page", page);
        }
        catch (NumberFormatException ex) {
            LOGGER.info("Couldn't parse " + request.getParameter("p") + ", "
                    + request.getParameter("s") +" to long");
        }

        return resultPage;
    }
}

