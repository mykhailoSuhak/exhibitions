package com.exhibitions.command.admin.exhibition;

import com.exhibitions.command.ServletCommand;
import com.exhibitions.dao.exhibition.MysqlExhibitionDaoImpl;
import com.exhibitions.dao.hall.MysqlHallDaoImpl;
import com.exhibitions.model.Exhibition;
import com.exhibitions.model.Hall;
import com.exhibitions.model.UserType;
import com.exhibitions.properties.MappingProperties;
import com.exhibitions.service.exhibition.ExhibitionService;
import com.exhibitions.service.exhibition.ExhibitionServiceImpl;
import com.exhibitions.service.hall.HallService;
import com.exhibitions.service.hall.HallServiceImpl;
import com.exhibitions.util.Page;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * This class is used to handle GET requests to delete magazines.
 */
public class DeleteExhibitionAdminCommand implements ServletCommand {
    private static final Logger LOGGER = Logger.getLogger(DeleteExhibitionAdminCommand.class);

    private static ExhibitionService exhibitionService;
    private static HallService hallService;

    private static String exhibitionsPage;
    private static String loginPage;

    public DeleteExhibitionAdminCommand(){
        LOGGER.info("Initializing DeleteExhibitionAdminCommand");

        exhibitionService = new ExhibitionServiceImpl(MysqlExhibitionDaoImpl.getInstance());
        hallService = new HallServiceImpl(MysqlHallDaoImpl.getInstance());

        MappingProperties properties = MappingProperties.getInstance();
        exhibitionsPage = properties.getProperty("adminExhibitionsPage");
        loginPage = properties.getProperty("loginPage");
    }

    public String execute(HttpServletRequest request, HttpServletResponse response, String locale) {
        LOGGER.info("Executing DeleteExhibitionAdminCommand");
        String resultPage = exhibitionsPage;

        if (request.getSession().getAttribute("authenticated") == null ||
                request.getSession().getAttribute("authenticated").equals(false) ||
                !request.getSession().getAttribute("role").equals(UserType.ADMIN.name())) {
            LOGGER.info("User not authorized");
            resultPage = loginPage;
        }
        else if(request.getParameter("id") != null) {
            try {
                Long id = Long.parseLong(request.getParameter("id"));

                hallService.removeHallsForExhibition(id);

                request.setAttribute("deletionSuccess", exhibitionService.deleteExhibitionById(id));
                Page<Exhibition> page = exhibitionService.getPage(1, 10, locale);

                request.setAttribute("page", page);
            }
            catch (NumberFormatException ex) {
                LOGGER.info("Couldn't parse " + request.getParameter("id") + " to long");
            }
        }

        return resultPage;
    }
}

