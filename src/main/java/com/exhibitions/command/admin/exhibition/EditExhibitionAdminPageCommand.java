package com.exhibitions.command.admin.exhibition;

import com.exhibitions.command.ServletCommand;
import com.exhibitions.dao.category.MysqlCategoryDaoImpl;
import com.exhibitions.dao.exhibition.MysqlExhibitionDaoImpl;
import com.exhibitions.dao.hall.MysqlHallDaoImpl;
import com.exhibitions.model.Exhibition;
import com.exhibitions.model.UserType;
import com.exhibitions.properties.MappingProperties;
import com.exhibitions.service.category.CategoryService;
import com.exhibitions.service.category.CategoryServiceImpl;
import com.exhibitions.service.exhibition.ExhibitionService;
import com.exhibitions.service.exhibition.ExhibitionServiceImpl;
import com.exhibitions.service.hall.HallService;
import com.exhibitions.service.hall.HallServiceImpl;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * This class is used to handle GET requests to the admin page used to edit magazine.
 */
public class EditExhibitionAdminPageCommand implements ServletCommand {
    private static final Logger LOGGER = Logger.getLogger(EditExhibitionAdminPageCommand.class);

    private static ExhibitionService exhibitionService;
    private static HallService hallService;
    private static CategoryService categoryService;

    private static String editExhibitionPage;
    private static String exhibitionsPage;
    private static String loginPage;

    public EditExhibitionAdminPageCommand(){
        LOGGER.info("Initializing EditExhibitionAdminPageCommand");

        exhibitionService = new ExhibitionServiceImpl(MysqlExhibitionDaoImpl.getInstance());
        hallService = new HallServiceImpl(MysqlHallDaoImpl.getInstance());
        categoryService = new CategoryServiceImpl(MysqlCategoryDaoImpl.getInstance());

        MappingProperties properties = MappingProperties.getInstance();
        editExhibitionPage = properties.getProperty("adminEditExhibitionPage");
        exhibitionsPage = properties.getProperty("adminExhibitionsPage");
        loginPage = properties.getProperty("loginPage");
    }

    public String execute(HttpServletRequest request, HttpServletResponse response, String locale) {
        LOGGER.info("Executing EditExhibitionAdminPageCommand");
        String resultPage = editExhibitionPage;

        if (request.getSession().getAttribute("authenticated") == null ||
                request.getSession().getAttribute("authenticated").equals(false) ||
                !request.getSession().getAttribute("role").equals(UserType.ADMIN.name())) {
            LOGGER.info("User not authorized");
            resultPage = loginPage;
        }
        else if(request.getParameter("id") != null) {
            try {
                Long id = Long.parseLong(request.getParameter("id"));

                Exhibition exhibition = exhibitionService.findExhibitionById(id, locale);

                if(exhibition != null) {
                    request.setAttribute("exhibition", exhibition);
                    request.setAttribute("categories", categoryService.findAll(locale));
                    request.setAttribute("halls", hallService.findAll(locale));
                }
                else {
                    resultPage = exhibitionsPage;
                }
            }
            catch (NumberFormatException ex) {
                LOGGER.info("Couldn't parse " + request.getParameter("id") + " to long");
            }

        }

        return resultPage;
    }
}

