package com.exhibitions.command.admin.exhibition;

import com.exhibitions.command.ServletCommand;
import com.exhibitions.dao.category.MysqlCategoryDaoImpl;
import com.exhibitions.dao.exhibition.MysqlExhibitionDaoImpl;
import com.exhibitions.dao.hall.MysqlHallDaoImpl;
import com.exhibitions.model.*;
import com.exhibitions.properties.MappingProperties;
import com.exhibitions.service.category.CategoryService;
import com.exhibitions.service.category.CategoryServiceImpl;
import com.exhibitions.service.exhibition.ExhibitionService;
import com.exhibitions.service.exhibition.ExhibitionServiceImpl;
import com.exhibitions.service.hall.HallService;
import com.exhibitions.service.hall.HallServiceImpl;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * This class is used to handle POST requests to add new exhibition.
 */
public class AddExhibitionAdminCommand implements ServletCommand {
    private static final Logger LOGGER = Logger.getLogger(AddExhibitionAdminCommand.class);

    private static ExhibitionService exhibitionService;
    private static HallService hallService;
    private static CategoryService categoryService;

    private static String addExhibitionPage;
    private static String loginPage;

    public AddExhibitionAdminCommand() {
        LOGGER.info("Initializing AddExhibitionAdminCommand");

        exhibitionService = new ExhibitionServiceImpl(MysqlExhibitionDaoImpl.getInstance());
        hallService = new HallServiceImpl(MysqlHallDaoImpl.getInstance());
        categoryService = new CategoryServiceImpl(MysqlCategoryDaoImpl.getInstance());

        MappingProperties properties = MappingProperties.getInstance();
        addExhibitionPage = properties.getProperty("adminAddExhibitionPage");
        loginPage = properties.getProperty("loginPage");
    }

    public String execute(HttpServletRequest request, HttpServletResponse response, String locale) {
        LOGGER.info("Executing AddExhibitionAdminCommand");
        String resultPage = addExhibitionPage;
        if (request.getSession().getAttribute("authenticated") == null ||
                request.getSession().getAttribute("authenticated").equals(false) ||
                !request.getSession().getAttribute("role").equals(UserType.ADMIN.name())) {
            LOGGER.info("User not authorized");
            resultPage = loginPage;
        } else if (request.getParameter("title") != null && request.getParameter("price") != null &&
                request.getParameter("titleUkr") != null && request.getParameter("category") != null &&
                request.getParameter("description") != null) {
            try {
                Part filePart = request.getPart("image");
                InputStream image = null;
                if (filePart != null && !Paths.get(filePart.getSubmittedFileName()).getFileName().toString().isEmpty()) {
                    image = filePart.getInputStream();
                }

                Category category = new Category();
                category.setId(Long.parseLong(request.getParameter("category")));

                List<Hall> halls = hallService.findAll(locale);
                List<Hall> exhibitionHalls = new ArrayList<>();

                for (Hall hall: halls) {
                    if (request.getParameter(String.valueOf(hall.getId())) != null)
                        exhibitionHalls.add(hall);
                }


                Exhibition exhibition = new ExhibitionBuilder().setTitle(request.getParameter("title"))
                        .setTitle_ukr(request.getParameter("titleUkr"))
                        .setDescription(request.getParameter("description"))
                        .setDescription_ukr(request.getParameter("descriptionUkr"))
                        .setPrice(Float.parseFloat(request.getParameter("price")))
                        .setCategory(category)
                        .setStartDate(LocalDate.parse(request.getParameter("startDate")))
                        .setEndDate(LocalDate.parse(request.getParameter("endDate")))
                        .build();

                exhibitionService.createExhibition(exhibition, image);

                hallService.setHallsForExhibition(exhibition.getId(), exhibitionHalls);

                request.setAttribute("halls", hallService.findAll(locale));
                request.setAttribute("categories", categoryService.findAll(locale));
            } catch (NumberFormatException ex) {
                LOGGER.info("Couldn't parse " + request.getParameter("id")
                        + ", "
                        + request.getParameter("category")
                        + ", "
                        + request.getParameter("publisher")
                        + " to long");
            } catch (ServletException | IOException e) {
                LOGGER.info(e.getMessage());
            }
        }

        return resultPage;
    }
}
