package com.exhibitions.command.admin.ticket;


import com.exhibitions.command.ServletCommand;
import com.exhibitions.dao.ticket.MysqlTicketDaoImpl;
import com.exhibitions.model.Ticket;
import com.exhibitions.model.UserType;
import com.exhibitions.properties.MappingProperties;
import com.exhibitions.service.ticket.TicketService;
import com.exhibitions.service.ticket.TicketServiceImpl;
import com.exhibitions.util.Page;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * This class is used to handle GET requests to the admin page used to display subscriptions.
 */
public class TicketsAdminPageCommand implements ServletCommand {
    private static final Logger LOGGER = Logger.getLogger(TicketsAdminPageCommand.class);

    private static TicketService ticketService;

    private static String resultPage;
    private static String loginPage;

    public TicketsAdminPageCommand() {
        LOGGER.info("Initializing TicketsAdminPageCommand");

        ticketService = new TicketServiceImpl(MysqlTicketDaoImpl.getInstance());

        MappingProperties properties = MappingProperties.getInstance();
        resultPage = properties.getProperty("adminTicketPage");
        loginPage = properties.getProperty("loginPage");
    }

    public String execute(HttpServletRequest request, HttpServletResponse response, String locale) {
        LOGGER.info("Executing TicketsAdminPageCommand");
        if (request.getSession().getAttribute("authenticated") == null ||
                request.getSession().getAttribute("authenticated").equals(false) ||
                !request.getSession().getAttribute("role").equals(UserType.ADMIN.name())) {
            LOGGER.info("User not authorized");
            resultPage = loginPage;
        } else
            try {
                Integer pageNum = Integer.parseInt(request.getParameter("p"));
                Integer size = Integer.parseInt(request.getParameter("s"));

                Page<Ticket> page = ticketService.getPage(pageNum, size, locale);

                request.setAttribute("page", page);
            } catch (NumberFormatException ex) {
                LOGGER.info("Couldn't parse " + request.getParameter("p") + ", "
                        + request.getParameter("s") + " to long");
            }

        return resultPage;
    }
}

