package com.exhibitions.command.admin.user;

import com.exhibitions.command.ServletCommand;
import com.exhibitions.dao.ticket.MysqlTicketDaoImpl;
import com.exhibitions.dao.user.MysqlUserDaoImpl;
import com.exhibitions.model.Ticket;
import com.exhibitions.model.User;
import com.exhibitions.model.UserType;
import com.exhibitions.properties.MappingProperties;
import com.exhibitions.service.ticket.TicketService;
import com.exhibitions.service.ticket.TicketServiceImpl;
import com.exhibitions.service.user.UserService;
import com.exhibitions.service.user.UserServiceImpl;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class GetUserInfoAdminCommand implements ServletCommand {

    private static final Logger LOGGER = Logger.getLogger(GetUserInfoAdminCommand.class);

    private static UserService userService;
    private static TicketService ticketService;

    private static String usersPage;
    private static String userInfoPage;
    private static String loginPage;

    public GetUserInfoAdminCommand() {
        LOGGER.info("Initializing GetUserInfoAdminCommand");

        userService = new UserServiceImpl(MysqlUserDaoImpl.getInstance());
        ticketService = new TicketServiceImpl(MysqlTicketDaoImpl.getInstance());

        MappingProperties properties = MappingProperties.getInstance();
        usersPage = properties.getProperty("adminUsersPage");
        userInfoPage = properties.getProperty("adminUserInfoPage");
        loginPage = properties.getProperty("loginPage");
    }

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response, String locale) {
        LOGGER.info("Executing GetUserInfoAdminCommand");
        String resultPage = usersPage;

        if (request.getSession().getAttribute("authenticated") == null ||
                request.getSession().getAttribute("authenticated").equals(false) ||
                !request.getSession().getAttribute("role").equals(UserType.ADMIN.name())) {
            LOGGER.info("User not authorized");
            resultPage = loginPage;
        } else if (request.getParameter("id") != null) {
            try {
                Long id = Long.parseLong(request.getParameter("id"));
                User user = userService.findUserById(id);

                if (user != null) {
                    List<Ticket> subscriptions = ticketService.getUserTickets(user.getId(), locale);

                    request.setAttribute("user", user);
                    request.setAttribute("isSubscriptionsEmpty", subscriptions.isEmpty());
                    request.setAttribute("subscriptions", subscriptions);
                    resultPage = userInfoPage;
                }
            } catch (NumberFormatException ex) {
                LOGGER.info("Couldn't parse id " + request.getParameter("p") + " to long");
            }
        }

        return resultPage;
    }
}
