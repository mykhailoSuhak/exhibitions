package com.exhibitions.command.admin.user;

import com.exhibitions.command.ServletCommand;
import com.exhibitions.dao.user.MysqlUserDaoImpl;
import com.exhibitions.model.User;
import com.exhibitions.model.UserBuilder;
import com.exhibitions.model.UserType;
import com.exhibitions.properties.MappingProperties;
import com.exhibitions.service.user.UserService;
import com.exhibitions.service.user.UserServiceImpl;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * This class is used to handle POST requests to create admin.
 */
public class AddAdminAdminCommand implements ServletCommand {

    private static final Logger LOGGER = Logger.getLogger(AddAdminAdminCommand.class);

    private static UserService userService;

    private static String addAdminPage;
    private static String loginPage;

    public AddAdminAdminCommand() {
        LOGGER.info("Initializing AddAdminAdminCommand");

        userService = new UserServiceImpl(MysqlUserDaoImpl.getInstance());

        MappingProperties properties = MappingProperties.getInstance();
        addAdminPage = properties.getProperty("adminAddAdminPage");
        loginPage = properties.getProperty("loginPage");
    }

    public String execute(HttpServletRequest request, HttpServletResponse response, String locale) {
        LOGGER.info("Executing AddAdminAdminCommand");
        String resultPage = addAdminPage;
        if (request.getSession().getAttribute("authenticated") == null ||
                request.getSession().getAttribute("authenticated").equals(false) ||
                !request.getSession().getAttribute("role").equals(UserType.ADMIN.name())) {
            LOGGER.info("User not authorized");
            resultPage = loginPage;
        } else if (request.getParameter("name") != null && request.getParameter("nameUkr") != null &&
                request.getParameter("email") != null && request.getParameter("password") != null &&
                userService.checkEmailAvailability(request.getParameter("email"))) {

            User user = new UserBuilder().setName(request.getParameter("name"))
                    .setName_ukr(request.getParameter("nameUkr"))
                    .setEmail(request.getParameter("email"))
                    .setPassword(request.getParameter("password"))
                    .setUserType(UserType.ADMIN)
                    .build();

            request.setAttribute("success", userService.registerUser(user));
        }

        return resultPage;
    }
}

