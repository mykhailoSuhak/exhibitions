package com.exhibitions.command.admin.user;

import com.exhibitions.command.ServletCommand;
import com.exhibitions.model.UserType;
import com.exhibitions.properties.MappingProperties;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * This class is used to handle GET requests to the page to create admin.
 */
public class GetAddAdminPageCommand implements ServletCommand {

    private static final Logger LOGGER = Logger.getLogger(GetAddAdminPageCommand.class);

    private static String addAdminPage;
    private static String loginPage;

    public GetAddAdminPageCommand(){
        LOGGER.info("Initializing GetAddAdminPageCommand");

        MappingProperties properties = MappingProperties.getInstance();
        addAdminPage = properties.getProperty("adminAddAdminPage");
        loginPage = properties.getProperty("loginPage");
    }

    public String execute(HttpServletRequest request, HttpServletResponse response, String locale) {
        LOGGER.info("Executing GetAddAdminPageCommand");
        String resultPage = addAdminPage;

        if (request.getSession().getAttribute("authenticated") == null ||
                request.getSession().getAttribute("authenticated").equals(false) ||
                !request.getSession().getAttribute("role").equals(UserType.ADMIN.name())) {
            LOGGER.info("User not authorized");
            resultPage = loginPage;
        }

        return resultPage;
    }
}

