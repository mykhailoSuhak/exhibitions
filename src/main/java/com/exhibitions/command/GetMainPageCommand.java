package com.exhibitions.command;

import com.exhibitions.dao.category.MysqlCategoryDaoImpl;
import com.exhibitions.dao.exhibition.MysqlExhibitionDaoImpl;
import com.exhibitions.properties.MappingProperties;
import com.exhibitions.service.category.CategoryService;
import com.exhibitions.service.category.CategoryServiceImpl;
import com.exhibitions.service.exhibition.ExhibitionService;
import com.exhibitions.service.exhibition.ExhibitionServiceImpl;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * This class is used to handle GET requests to the main page.
 */
public class GetMainPageCommand implements ServletCommand {

    private static final Logger LOGGER = Logger.getLogger(GetMainPageCommand.class);

    private static CategoryService categoryService;
    private static ExhibitionService exhibitionService;

    private static String page;

    public GetMainPageCommand(){
        LOGGER.info("Initializing GetMainPageCommand");

        categoryService = new CategoryServiceImpl(MysqlCategoryDaoImpl.getInstance());
        exhibitionService = new ExhibitionServiceImpl(MysqlExhibitionDaoImpl.getInstance());

        MappingProperties properties = MappingProperties.getInstance();
        page = properties.getProperty("mainPage");
    }

    public String execute(HttpServletRequest request, HttpServletResponse response, String locale) {
        LOGGER.info("Executing GetMainPageCommand");

        request.setAttribute("categories", categoryService.findAll(locale));


        request.setAttribute("latestExhibitions", exhibitionService.findLatestAdded(6, locale));
        LOGGER.info(page);

        return page;
    }
}
