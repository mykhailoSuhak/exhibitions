package com.exhibitions.command;

import com.exhibitions.dao.exhibition.MysqlExhibitionDaoImpl;
import com.exhibitions.dao.ticket.TicketDao;
import com.exhibitions.dao.ticket.MysqlTicketDaoImpl;
import com.exhibitions.dao.user.MysqlUserDaoImpl;
import com.exhibitions.model.Exhibition;
import com.exhibitions.properties.MappingProperties;
import com.exhibitions.service.exhibition.ExhibitionService;
import com.exhibitions.service.exhibition.ExhibitionServiceImpl;
import com.exhibitions.service.ticket.TicketService;
import com.exhibitions.service.ticket.TicketServiceImpl;
import com.exhibitions.service.user.UserService;
import com.exhibitions.service.user.UserServiceImpl;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * This class is used to handle GET requests to the buyTicket page.
 */
public class GetBuyTicketCommand implements ServletCommand {

    private static final Logger LOGGER = Logger.getLogger(GetBuyTicketCommand.class);

    private static ExhibitionService exhibitionService;
    private static TicketService ticketService;
    private static UserService userService;

    private static String buyTicketPage;
    private static String mainPage;
    private static String loginPage;
    private static String buyTicketSuccessPage;

    public GetBuyTicketCommand(){
        LOGGER.info("Initializing GetBuyTicketCommand");

        exhibitionService = new ExhibitionServiceImpl(MysqlExhibitionDaoImpl.getInstance());
        ticketService = new TicketServiceImpl(MysqlTicketDaoImpl.getInstance());
        userService = new UserServiceImpl(MysqlUserDaoImpl.getInstance());

        MappingProperties properties = MappingProperties.getInstance();
        buyTicketPage = properties.getProperty("buyTicketPage");
        mainPage = properties.getProperty("mainPage");
        loginPage = properties.getProperty("loginPage");
        buyTicketSuccessPage = properties.getProperty("buyTicketSuccessPage");
    }

    public String execute(HttpServletRequest request, HttpServletResponse response, String locale) {
        LOGGER.info("Executing GetBuyTicketCommand");
        String resultPage = mainPage;

        if(request.getSession().getAttribute("authenticated") == null &&
                request.getSession().getAttribute("authenticated").equals(true)) {
            resultPage = loginPage;
        }
        else if(request.getParameter("id") != null) {
            try {
                Long id = Long.parseLong(request.getParameter("id"));
                Exhibition exhibition = exhibitionService.findExhibitionById(id, locale);

                if(exhibition != null) {
                    request.setAttribute("exhibition", exhibition);
                    resultPage = buyTicketPage;
                }
                else {
                    LOGGER.info("Magazine with id " + id + " doesn't exist");
                }
            }
            catch (NumberFormatException ex) {
                LOGGER.info("Couldn't parse " + request.getParameter("id") + " to long");
            }
        }

        return resultPage;
    }
}
