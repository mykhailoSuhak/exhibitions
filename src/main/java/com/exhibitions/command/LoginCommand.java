package com.exhibitions.command;

import com.exhibitions.dao.category.MysqlCategoryDaoImpl;
import com.exhibitions.dao.exhibition.MysqlExhibitionDaoImpl;
import com.exhibitions.dao.user.MysqlUserDaoImpl;
import com.exhibitions.model.User;
import com.exhibitions.model.UserType;
import com.exhibitions.properties.MappingProperties;
import com.exhibitions.service.category.CategoryService;
import com.exhibitions.service.category.CategoryServiceImpl;
import com.exhibitions.service.exhibition.ExhibitionService;
import com.exhibitions.service.exhibition.ExhibitionServiceImpl;
import com.exhibitions.service.user.UserService;
import com.exhibitions.service.user.UserServiceImpl;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Objects;

/**
 * This class is used to handle POST requests to authenticate users.
 */
public class LoginCommand implements ServletCommand {
    private static final Logger LOGGER = Logger.getLogger(LoginCommand.class);

    private static UserService userService;
    private static CategoryService categoryService;
    private static ExhibitionService exhibitionService;

    private static String loginPage;
    private static String mainPage;

    public LoginCommand(){
        LOGGER.info("Initializing LoginCommand");

        userService = new UserServiceImpl(MysqlUserDaoImpl.getInstance());
        categoryService = new CategoryServiceImpl(MysqlCategoryDaoImpl.getInstance());
        exhibitionService = new ExhibitionServiceImpl(MysqlExhibitionDaoImpl.getInstance());
        MappingProperties properties = MappingProperties.getInstance();
        loginPage = properties.getProperty("loginPage");
        mainPage = properties.getProperty("mainPage");
    }

    public String execute(HttpServletRequest request, HttpServletResponse response, String locale) {
        LOGGER.info("Executing command");

        String resultPage = loginPage;

        if (request.getParameter("email") != null && request.getParameter("password") != null) {
            User user = userService.getUserByCredentials(request.getParameter("email"),
                    request.getParameter("password"));

            if (user != null) {
                HttpSession session = request.getSession();
                session.setAttribute("email", user.getEmail());
                session.setAttribute("username", user.getName());
                session.setAttribute("usernameukr", user.getName_ukr());
                session.setAttribute("authenticated", true);
                session.setAttribute("role", user.getUserType().name());

                request.setAttribute("categories", categoryService.findAll(locale));
                request.setAttribute("latestExhibitions", exhibitionService.findLatestAdded(6, locale));

                resultPage = mainPage;
            }
            else {
                request.setAttribute("loginSuccess", false);
            }
        }

        return resultPage;
    }
}
