package com.exhibitions.command;

import com.exhibitions.dao.category.MysqlCategoryDaoImpl;
import com.exhibitions.dao.exhibition.MysqlExhibitionDaoImpl;
import com.exhibitions.dao.user.MysqlUserDaoImpl;
import com.exhibitions.model.User;
import com.exhibitions.model.UserBuilder;
import com.exhibitions.model.UserType;
import com.exhibitions.properties.MappingProperties;
import com.exhibitions.service.category.CategoryService;
import com.exhibitions.service.category.CategoryServiceImpl;
import com.exhibitions.service.exhibition.ExhibitionService;
import com.exhibitions.service.exhibition.ExhibitionServiceImpl;
import com.exhibitions.service.user.UserService;
import com.exhibitions.service.user.UserServiceImpl;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * This class is used to handle POST requests to register user.
 */
public class RegisterCommand implements ServletCommand {

    private static final Logger LOGGER = Logger.getLogger(RegisterCommand.class);

    private static UserService userService;
    private static CategoryService categoryService;
    private static ExhibitionService exhibitionService;

    private static String registerPage;
    private static String mainPage;

    public RegisterCommand() {
        LOGGER.info("Initializing RegisterCommand");

        userService = new UserServiceImpl(MysqlUserDaoImpl.getInstance());
        categoryService = new CategoryServiceImpl(MysqlCategoryDaoImpl.getInstance());
        exhibitionService = new ExhibitionServiceImpl(MysqlExhibitionDaoImpl.getInstance());

        MappingProperties properties = MappingProperties.getInstance();
        registerPage = properties.getProperty("registerPage");
        mainPage = properties.getProperty("mainPage");
    }

    public String execute(HttpServletRequest request, HttpServletResponse response, String locale) {
        LOGGER.info("Executing register command");

        String resultPage = registerPage;

        if (request.getParameter("name") != null && request.getParameter("name_ukr") != null &&
                request.getParameter("email") != null && request.getParameter("password") != null &&
                userService.checkEmailAvailability(request.getParameter("email"))) {

            LOGGER.info("New user registration");

            User user = new UserBuilder().setName(request.getParameter("name"))
                    .setName_ukr(request.getParameter("name_ukr"))
                    .setEmail(request.getParameter("email"))
                    .setPassword(request.getParameter("password"))
                    .setUserType(UserType.USER)
                    .build();

            if (userService.registerUser(user)) {
                request.setAttribute("categories", categoryService.findAll(locale));
                request.setAttribute("latestExhibitions", exhibitionService.findLatestAdded(6, locale));

                resultPage = mainPage;
            }
        }

        return resultPage;
    }
}

