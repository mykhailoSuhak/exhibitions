package com.exhibitions.command;

import com.exhibitions.dao.category.MysqlCategoryDaoImpl;
import com.exhibitions.dao.exhibition.MysqlExhibitionDaoImpl;
import com.exhibitions.dao.ticket.MysqlTicketDaoImpl;
import com.exhibitions.dao.user.MysqlUserDaoImpl;
import com.exhibitions.model.Exhibition;
import com.exhibitions.model.User;
import com.exhibitions.properties.MappingProperties;
import com.exhibitions.service.category.CategoryService;
import com.exhibitions.service.category.CategoryServiceImpl;
import com.exhibitions.service.exhibition.ExhibitionService;
import com.exhibitions.service.exhibition.ExhibitionServiceImpl;
import com.exhibitions.service.ticket.TicketService;
import com.exhibitions.service.ticket.TicketServiceImpl;
import com.exhibitions.service.user.UserService;
import com.exhibitions.service.user.UserServiceImpl;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * This class is used to handle GET requests to the page used to view a single exhibition.
 */
public class GetExhibitionPageCommand implements ServletCommand {

    private static final Logger LOGGER = Logger.getLogger(GetExhibitionPageCommand.class);

    private static CategoryService categoryService;
    private static ExhibitionService exhibitionService;
    private static UserService userService;
    private static TicketService ticketService;

    private static String exhibitionPage;
    private static String errorPage;

    public GetExhibitionPageCommand() {
        LOGGER.info("Initializing GetExhibitionPageCommand");

        categoryService = new CategoryServiceImpl(MysqlCategoryDaoImpl.getInstance());
        exhibitionService = new ExhibitionServiceImpl(MysqlExhibitionDaoImpl.getInstance());
        userService = new UserServiceImpl(MysqlUserDaoImpl.getInstance());
        ticketService = new TicketServiceImpl(MysqlTicketDaoImpl.getInstance());

        MappingProperties properties = MappingProperties.getInstance();
        exhibitionPage = properties.getProperty("exhibitionPage");
        errorPage = properties.getProperty("error404Page");
    }

    public String execute(HttpServletRequest request, HttpServletResponse response, String locale) {
        LOGGER.info("Executing command");
        String resultPage = errorPage;

        if (request.getParameter("id") != null) {
            try {
                Long id = Long.parseLong(request.getParameter("id"));
                Exhibition exhibition = exhibitionService.findExhibitionById(id, locale);

                if (exhibition != null) {
                    if (request.getSession().getAttribute("authenticated") != null &&
                            request.getSession().getAttribute("authenticated").equals(true)) {
                        User user = userService.findUserByEmail(request.getSession().getAttribute("email").toString());
                        boolean hasTicket = ticketService.checkIfUserHasTicket(user, exhibition);

                        request.setAttribute("isSubscribed", hasTicket);
                    }

                    request.setAttribute("categories", categoryService.findAll(locale));
                    request.setAttribute("exhibition", exhibition);
                    request.setAttribute("count", ticketService.getNumberOfSoldTickets(id));


                    resultPage = exhibitionPage;
                } else {
                    LOGGER.info("Exhibition with id " + id + " doesn't exist");
                }
            } catch (NumberFormatException ex) {
                LOGGER.info("Couldn't parse " + request.getParameter("id") + " to long");
            }
        }

        return resultPage;
    }
}

