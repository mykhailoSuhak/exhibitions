package com.exhibitions.command;

import com.exhibitions.dao.exhibition.MysqlExhibitionDaoImpl;
import com.exhibitions.dao.ticket.MysqlTicketDaoImpl;
import com.exhibitions.dao.user.MysqlUserDaoImpl;
import com.exhibitions.model.Exhibition;
import com.exhibitions.model.Ticket;
import com.exhibitions.model.User;
import com.exhibitions.properties.MappingProperties;
import com.exhibitions.service.exhibition.ExhibitionService;
import com.exhibitions.service.exhibition.ExhibitionServiceImpl;
import com.exhibitions.service.ticket.TicketService;
import com.exhibitions.service.ticket.TicketServiceImpl;
import com.exhibitions.service.user.UserService;
import com.exhibitions.service.user.UserServiceImpl;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.Date;
import java.util.Calendar;

/**
 * This class is used to handle POST requests to let user buy ticket to the exhibition.
 */
public class BuyTicketCommand implements ServletCommand {

    private static final Logger LOGGER = Logger.getLogger(BuyTicketCommand.class);

    private static ExhibitionService exhibitionService;
    private static TicketService ticketService;
    private static UserService userService;

    private static String mainPage;
    private static String buyTicketSuccessPage;

    public BuyTicketCommand(){
        LOGGER.info("Initializing BuyTicketCommand");

        exhibitionService = new ExhibitionServiceImpl(MysqlExhibitionDaoImpl.getInstance());
        ticketService = new TicketServiceImpl(MysqlTicketDaoImpl.getInstance());
        userService = new UserServiceImpl(MysqlUserDaoImpl.getInstance());

        MappingProperties properties = MappingProperties.getInstance();
        mainPage = properties.getProperty("mainPage");
        buyTicketSuccessPage = properties.getProperty("buyTicketSuccessPage");
    }

    public String execute(HttpServletRequest request, HttpServletResponse response, String locale) {
        LOGGER.info("Executing BuyTicketCommand");
        String resultPage = mainPage;

        if(request.getParameter("id") != null) {
            try {
                Long exhibitionId = Long.parseLong(request.getParameter("id"));

                Exhibition exhibition = exhibitionService.findExhibitionById(exhibitionId, locale);
                User user = userService.findUserByEmail(request.getSession().getAttribute("email").toString());

                if(exhibition != null) {
                    Ticket ticket = new Ticket();

                    ticket.setExhibition(exhibition);
                    ticket.setPrice(exhibition.getPrice());
                    ticket.setUser(user);

                    ticketService.createTicket(ticket);

                    resultPage = buyTicketSuccessPage;
                }
                else {
                    LOGGER.info("Couldn't find exhibition by id " + exhibition);
                }

            }
            catch (NumberFormatException ex) {
                LOGGER.info("Couldn't parse " + request.getParameter("id") + " to long");
            }
        }

        return resultPage;
    }
}
