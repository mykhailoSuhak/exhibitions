package com.exhibitions.command;

import com.exhibitions.command.admin.category.*;
import com.exhibitions.command.admin.exhibition.*;
import com.exhibitions.command.admin.hall.*;
import com.exhibitions.command.admin.ticket.TicketsAdminPageCommand;
import com.exhibitions.command.admin.user.*;
import com.exhibitions.properties.MappingProperties;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;

/**
 * This is a helper class that is used to work with servlet commands.
 *
 * It handles mapping of the url paths to the commands.
 */
public class CommandManager {

    private static final Logger LOGGER = Logger.getLogger(CommandManager.class);

    private HashMap<String, ServletCommand> getCommands;
    private HashMap<String, ServletCommand> postCommands;
    private static String errorPage;

    public CommandManager(){
        LOGGER.info("Initializing CommandManager");

        getCommands = new HashMap<>();
        postCommands = new HashMap<>();

        //===================GET commands===================

        getCommands.put("/", new GetMainPageCommand());
        getCommands.put("/register", new GetRegisterPageCommand());
        getCommands.put("/login", new GetLoginPageCommand());
        getCommands.put("/logout", new LogoutCommand());
        getCommands.put("/category", new GetCategoryPageCommand());
        getCommands.put("/byTime", new ByTimePageCommand());
        getCommands.put("/byPrice", new ByPricePageCommand());


        getCommands.put("/exhibition", new GetExhibitionPageCommand());
        getCommands.put("/buyTicket", new GetBuyTicketCommand());
        getCommands.put("/account", new GetAccountPageCommand());
        getCommands.put("/search", new GetSearchPageCommand());
        getCommands.put("/image", new GetImageCommand());


        //admin categories
        getCommands.put("/admin/categories", new CategoriesAdminPageCommand());
        getCommands.put("/admin/categories/add", new AddCategoryAdminPageCommand());
        getCommands.put("/admin/categories/delete", new DeleteCategoryAdminCommand());
        getCommands.put("/admin/categories/edit", new EditCategoryAdminPageCommand());

        //admin publishers
        getCommands.put("/admin/halls", new HallsAdminPageCommand());
        getCommands.put("/admin/halls/edit", new EditHallAdminPageCommand());
        getCommands.put("/admin/halls/add", new AddHallAdminPageCommand());
        getCommands.put("/admin/halls/delete", new DeleteHallAdminCommand());

        //admin users
        getCommands.put("/admin/users", new UsersAdminPageCommand());
        getCommands.put("/admin/admins", new AdminsAdminPageCommand());
        getCommands.put("/admin/admins/add", new GetAddAdminPageCommand());
        getCommands.put("/admin/user", new GetUserInfoAdminCommand());

        //admin exhibitions
        getCommands.put("/admin/exhibitions", new ExhibitionsAdminPageCommand());
        getCommands.put("/admin/exhibitions/add", new GetAddExhibitionAdminPageCommand());
        getCommands.put("/admin/exhibitions/edit", new EditExhibitionAdminPageCommand());
        getCommands.put("/admin/exhibitions/delete", new DeleteExhibitionAdminCommand());

        //admin tickets
        getCommands.put("/admin/tickets", new TicketsAdminPageCommand());

        //===================POST commands===================

        postCommands.put("/login", new LoginCommand());
        postCommands.put("/register", new RegisterCommand());
        postCommands.put("/byTime", new ByTimePageCommand());
        postCommands.put("/byPrice", new ByPricePageCommand());
        postCommands.put("/buyticket", new BuyTicketCommand());

        //admin categories
        postCommands.put("/admin/categories/add", new AddCategoryAdminCommand());
        postCommands.put("/admin/categories/update", new UpdateCategoryAdminCommand());

        //admin publishers
        postCommands.put("/admin/halls/add", new AddHallAdminCommand());
        postCommands.put("/admin/halls/update", new UpdateHallAdminCommand());

        //admin users
         postCommands.put("/admin/admins/add", new AddAdminAdminCommand());

        //admin exhibitions
        postCommands.put("/admin/exhibitions/add", new AddExhibitionAdminCommand());
        postCommands.put("/admin/exhibitions/update", new UpdateExhibitionAdminCommand());


        MappingProperties properties = MappingProperties.getInstance();
        errorPage = properties.getProperty("errorPage");
    }

    /**
     * This method is used to get a command instance mapped to http get method, based on a request.
     *
     * @param request http request from servlet.
     * @return        A servlet command instance.
     */
    public ServletCommand getGetCommand(HttpServletRequest request) {
        String command = getMapping(request);
        LOGGER.info("Getting command " + command);

        if(getCommands.get(command) == null) {
            return getCommands.get("/");
        }

        return getCommands.get(command);
    }

    /**
     * This method is used to get a command instance mapped to http post method, based on a request.
     *
     * @param request http request from servlet.
     * @return        A servlet command instance.
     */
    public ServletCommand getPostCommand(HttpServletRequest request) {
        String command = getMapping(request);
        LOGGER.info("Getting command " + command);

        if(postCommands.get(command) == null) {
            return getCommands.get("/");
        }
        return postCommands.get(command);
    }

    /**
     * This is a helper method to get command mapping from uri.
     *
     * @param request http request from servlet.
     * @return        Command mapping.
     */
    public String getMapping(HttpServletRequest request) {
        String mapping = request.getRequestURI().substring(request.getContextPath().length());
        if(mapping.endsWith("/")) {
            mapping = mapping.substring(0, mapping.length() - 1);
        }

        return mapping;
    }
}
