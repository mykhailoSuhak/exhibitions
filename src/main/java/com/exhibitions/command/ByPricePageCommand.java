package com.exhibitions.command;

import com.exhibitions.dao.category.MysqlCategoryDaoImpl;
import com.exhibitions.dao.exhibition.MysqlExhibitionDaoImpl;
import com.exhibitions.model.Exhibition;
import com.exhibitions.properties.MappingProperties;
import com.exhibitions.service.category.CategoryService;
import com.exhibitions.service.category.CategoryServiceImpl;
import com.exhibitions.service.exhibition.ExhibitionService;
import com.exhibitions.service.exhibition.ExhibitionServiceImpl;
import com.exhibitions.util.Page;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * This class is used to handle GET requests to the page to view all the exhibitions
 * by time, set by user.
 */
public class ByPricePageCommand implements ServletCommand {

    private static final Logger LOGGER = Logger.getLogger(ByPricePageCommand.class);

    private static CategoryService categoryService;
    private static ExhibitionService exhibitionService;

    private static String byPricePage;
    private static String errorPage;

    public ByPricePageCommand() {
        LOGGER.info("Initializing ByPricePageCommand");

        categoryService = new CategoryServiceImpl(MysqlCategoryDaoImpl.getInstance());
        exhibitionService = new ExhibitionServiceImpl(MysqlExhibitionDaoImpl.getInstance());

        MappingProperties properties = MappingProperties.getInstance();
        byPricePage = properties.getProperty("byPricePage");
        errorPage = properties.getProperty("error404Page");
    }

    public String execute(HttpServletRequest request, HttpServletResponse response, String locale) {
        LOGGER.info("Executing ByPricePageCommand");

        String resultPage = errorPage;
        HttpSession session = request.getSession();
        Boolean direction;

        if ( request.getParameter("p") != null &&
                request.getParameter("s") != null) {

            try {
                if (request.getParameter("order").equals("cheapFirst"))
                    direction = true;
                else direction = false;


                Integer pageNum = Integer.parseInt(request.getParameter("p"));
                Integer size = Integer.parseInt(request.getParameter("s"));

                Page<Exhibition> page = exhibitionService.getPageByPrice(pageNum, size, direction, locale);


                request.setAttribute("categories", categoryService.findAll(locale));

                request.setAttribute("page", page);
                request.setAttribute("order", request.getParameter("order"));

                resultPage = byPricePage;
            } catch (NumberFormatException ex) {
                LOGGER.info("Couldn't parse " + request.getParameter("catId") + ", "
                        + request.getParameter("p") + ", "
                        + request.getParameter("s") + " to long");
            }
        }

        return resultPage;
    }
}