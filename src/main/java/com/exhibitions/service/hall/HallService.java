package com.exhibitions.service.hall;

import com.exhibitions.model.Hall;

import java.util.List;

public interface HallService {
    /**
     * This method gets all halls.
     *
     * @return A list of all halls.
     */
    List<Hall> findAll(String locale);

    /**
     * This method gets hall by id.
     *
     * @param id Id of the hall to find.
     * @return   Hall object.
     */
    Hall findHallById(Long id);

    /**
     * This method creates new hall.
     *
     * @param hall Hall object to be created.
     * @return          Updated object.
     */
    Hall createHall(Hall hall);

    /**
     * This method deletes hall.
     *
     * @param id Id of the hall to delete.
     * @return   True if hall deleted successfully, otherwise false.
     */
    boolean deleteHallById(Long id);

    /**
     * This method removes all halls for exhibition.
     *
     * @param exhibitionId Id of the exhibition.
     * @return     True if halls were deleted successfully, otherwise false.
     */
    public boolean removeHallsForExhibition(Long exhibitionId);

    /**
     * This method updates hall.
     *
     * @param hall Object to be updated.
     * @return         An updated object.
     */
    Hall updateHall(Hall hall);

    /**
     * This method sets halls for exhibition.
     *
     * @param halls List of halls to be set.
     * @param exhibitionId Id of the exhibition.
     * @return True if halls are set successfully, otherwise false.
     */
    boolean setHallsForExhibition(Long exhibitionId, List<Hall> halls);
}
