package com.exhibitions.service.hall;

import com.exhibitions.dao.hall.MysqlHallDaoImpl;
import com.exhibitions.dao.hall.HallDao;
import com.exhibitions.model.Hall;
import org.apache.log4j.Logger;

import java.util.List;

public class HallServiceImpl implements HallService {

    private static final Logger LOGGER = Logger.getLogger(HallService.class);

    private HallDao hallDao;

    public HallServiceImpl(HallDao hallDao) {
        LOGGER.info("Initializing HallServiceImpl");

        this.hallDao = hallDao;
    }

    @Override
    public List<Hall> findAll(String locale) {
        LOGGER.info("Getting all halls");

        return hallDao.findAll(locale);
    }

    @Override
    public Hall findHallById(Long id) {
        LOGGER.info("Getting hall by id " + id);

        if(id == null) {
            return null;
        }

        return hallDao.findHallById(id);
    }

    @Override
    public Hall createHall(Hall hall) {
        LOGGER.info("Creating new hall");

        if(hallDao.findHallByTitle(hall.getTitle()) == null) {
            return hallDao.createHall(hall);
        }

        return null;
    }

    @Override
    public boolean deleteHallById(Long id) {
        LOGGER.info("Deleting hall by id " + id);

        return id != null && hallDao.deleteHallById(id);

    }

    @Override
    public boolean removeHallsForExhibition(Long exhibitionId) {
        LOGGER.info("Removing halls for exhibition " + exhibitionId);

        return exhibitionId != null && hallDao.removeHallsForExhibition(exhibitionId);

    }

    @Override
    public Hall updateHall(Hall hall) {
        LOGGER.info("Updating hall by id " + hall.getId());

        if(hall == null) {
            return null;
        }

        return hallDao.updateHall(hall);
    }

    @Override
    public boolean setHallsForExhibition(Long exhibitionId, List<Hall> halls) {
        LOGGER.info("Setting halls for exhibition id " + exhibitionId);

        if(halls == null || exhibitionId == null) {
            return false;
        }

        return hallDao.setHallsForExhibition(exhibitionId, halls);
    }
}
