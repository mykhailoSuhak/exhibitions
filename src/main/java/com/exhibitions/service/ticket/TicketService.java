package com.exhibitions.service.ticket;

import com.exhibitions.model.Exhibition;
import com.exhibitions.model.Ticket;
import com.exhibitions.model.User;
import com.exhibitions.util.Page;

import java.util.List;

public interface TicketService {

    /**
     * This method creates new ticket.
     *
     * @param ticket Ticket object to be created.
     * @return             Updated object.
     */
    Ticket createTicket(Ticket ticket);

    /**
     * This method returns weather user has ticket to an exhibition.
     *
     * @param user     User object to check.
     * @param exhibition Exhibition object to check.
     * @return         True if user has ticket, otherwise false.
     */
    boolean checkIfUserHasTicket(User user, Exhibition exhibition);

    /**
     * This method finds all tickets of user.
     *
     * @param userId Id of user.
     * @return       List of user tickets.
     */
    List<Ticket> getUserTickets(Long userId, String locale);

    /**
     * This method returns a page of all tickets.
     *
     * @param page Number of the page, starts from 1.
     * @param size Size of the page.
     * @return     A list of tickets.
     */
    Page<Ticket> getPage(Integer page, Integer size, String locale);

    /**
     * This methods finds quantity of sold tickets to the current exhibition.
     *
     * @param exhibitionId Id of the exhibition.
     * @return       number of sold tickets.
     */
    Integer getNumberOfSoldTickets (Long exhibitionId);
}
