package com.exhibitions.service.ticket;

import com.exhibitions.dao.ticket.MysqlTicketDaoImpl;
import com.exhibitions.dao.ticket.TicketDao;
import com.exhibitions.model.Exhibition;
import com.exhibitions.model.Ticket;
import com.exhibitions.model.User;
import com.exhibitions.util.Page;
import org.apache.log4j.Logger;

import java.util.List;

public class TicketServiceImpl implements TicketService {

    private static final Logger LOGGER = Logger.getLogger(TicketServiceImpl.class);

    private TicketDao ticketDao;

    public TicketServiceImpl(TicketDao ticketDao) {
        LOGGER.info("Initializing TicketServiceImpl");

        this.ticketDao = ticketDao;
    }

    @Override
    public Ticket createTicket(Ticket ticket) {
        LOGGER.info("Creating new ticket");

        if(ticket == null) {
            return null;
        }

        return ticketDao.createTicket(ticket);
    }

    @Override
    public boolean checkIfUserHasTicket(User user, Exhibition exhibition) {
        LOGGER.info("Checking if user has ticket to exhibition");

        if(user == null || exhibition == null) {
            return false;
        }

        return ticketDao.checkIfUserHasTicket(user.getId(), exhibition.getId());
    }

    @Override
    public List<Ticket> getUserTickets(Long userId, String locale) {
        LOGGER.info("Finding tickets by user id " + userId);

        if(userId == null) {
            return null;
        }

        return ticketDao.findTicketsByUserId(userId, locale);
    }

    @Override
    public Page<Ticket> getPage(Integer page, Integer size, String locale) {
        LOGGER.info("Getting page number " + page + ", of size " + size );

        if(page == null || size == null || page < 1 || size < 1) {
            return null;
        }

        List<Ticket> items = ticketDao.findPage((page - 1) * size, size, locale);
        return new Page<>(items, page, size);
    }

    public Integer getNumberOfSoldTickets(Long exhibitionId) {

        if (exhibitionId == null) {
            return null;
        }
        return ticketDao.numberOfSoldTickets(exhibitionId);
    }

}