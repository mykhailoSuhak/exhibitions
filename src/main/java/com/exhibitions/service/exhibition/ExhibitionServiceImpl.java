package com.exhibitions.service.exhibition;

import com.exhibitions.dao.exhibition.ExhibitionDao;
import com.exhibitions.dao.exhibition.MysqlExhibitionDaoImpl;
import com.exhibitions.model.Exhibition;
import com.exhibitions.util.Page;
import org.apache.log4j.Logger;

import java.io.InputStream;
import java.util.List;

public class ExhibitionServiceImpl implements ExhibitionService {

    private static final Logger LOGGER = Logger.getLogger(ExhibitionServiceImpl.class);

    private ExhibitionDao exhibitionDao;
    //private ImageDao imageDao;

    public ExhibitionServiceImpl(ExhibitionDao exhibitionDao) {
        LOGGER.info("Initializing ExhibitionServiceImpl");

        this.exhibitionDao = exhibitionDao;
      //  this.imageDao = imageDao;
    }

    @Override
    public Exhibition findExhibitionById(Long id, String locale) {
        LOGGER.info("Finding exhibition by id " + id);

        if(id == null) {
            return null;
        }

        return exhibitionDao.findExhibitionById(id, locale);
    }

    @Override
    public List<Exhibition> findLatestAdded(Integer limit, String locale) {
        LOGGER.info("Finding " + limit + " latest exhibitions");

        if(limit == null) {
            return null;
        }

        return exhibitionDao.findLastExhibitions(limit, locale);
    }

    @Override
    public Page<Exhibition> getPageByCategoryId(Integer page, Integer size, Long categoryId, String locale) {
        LOGGER.info("Getting page number " + page + ", of size " + size + ", for category id " + categoryId);

        if(page == null || size == null || categoryId == null || page < 1 || size < 1) {
            return null;
        }

        List<Exhibition> items = exhibitionDao.findPageByCategory(categoryId, (page - 1) * size, size, locale);
        return new Page<>(items, page, size);
    }


    @Override
    public Page<Exhibition> getPageByTime(Integer page, Integer size, String startdate, String enddate, String locale) {
        LOGGER.info("Getting page number " + page + ", of size " + size + ", for time between " + startdate + " and " + enddate);

        if(page == null || size == null || startdate == null || enddate == null || page < 1 || size < 1) {
            return null;
        }

        List<Exhibition> items = exhibitionDao.findPageByTime(startdate, enddate, (page - 1) * size, size, locale);
        return new Page<>(items, page, size);
    }

    @Override
    public Page<Exhibition> getPageByPrice(Integer page, Integer size, Boolean direction, String locale) {
        LOGGER.info("Getting page number " + page + ", of size " + size );

        if(page == null || size == null || direction == null || page < 1 || size < 1) {
            return null;
        }

        List<Exhibition> items = exhibitionDao.findPageByPrice(direction, (page - 1) * size, size, locale);
        return new Page<>(items, page, size);
    }



    @Override
    public Page<Exhibition> getPage(Integer page, Integer size, String locale) {
        LOGGER.info("Getting page number " + page + ", of size " + size );

        if(page == null || size == null || page < 1 || size < 1) {
            return null;
        }

        List<Exhibition> items = exhibitionDao.findPage((page - 1) * size, size, locale);
        Page<Exhibition> pages = new Page<>(items, page, size);
        LOGGER.info("Service items size before if + " + items.size());
        if (items.size()==size) {
            pages.setLast(true);
        }
        if (items.size()==size+1) {
            pages.setLast(false);
            items.remove(items.get(items.size()-1));
        }

        LOGGER.info("Service pages items size after if + " + pages.getItems().size());

        return pages;
    }

    @Override
    public Exhibition createExhibition(Exhibition exhibition, InputStream image) {
        LOGGER.info("Creating new exhibition");

        if(exhibition == null) {
            return null;
        }

        return exhibitionDao.createExhibition(exhibition, image);
    }

    @Override
    public Exhibition updateExhibition(Exhibition exhibition, InputStream image) {
        LOGGER.info("Updating exhibition");

        if(exhibition == null) {
            return null;
        }

        return exhibitionDao.updateExhibition(exhibition, image);
    }

    @Override
    public boolean deleteExhibitionById(Long id) {
        LOGGER.info("Deleting exhibition by id " + id);

        if(id == null) {
            return false;
        }


        return exhibitionDao.deleteExhibitionById(id);
    }

    @Override
    public Page<Exhibition> getPageByName(String query, Integer page, Integer size, String locale) {
        LOGGER.info("Getting page by query " + query + " number " + page + ", of size " + size );

        if(page == null || size == null || query == null || page < 1 || size < 1) {
            return null;
        }

        List<Exhibition> items = exhibitionDao.findPageByNameQuery(query, (page - 1) * size, size, locale);
        return new Page<>(items, page, size);
    }

    @Override
    public byte[] findImageById(Long id) {
        LOGGER.info("Finding magazine image by id " + id);

        if(id == null) {
            return null;
        }

        return exhibitionDao.findImageById(id);
    }

    @Override
    public Long createImage(InputStream image) {
        LOGGER.info("Creating new image");

        if(image == null) {
            return null;
        }

        return exhibitionDao.createImage(image);
    }

    @Override
    public boolean deleteImageById(Long id) {
        LOGGER.info("Deleting image by id " + id);

        if(id == null) {
            return false;
        }

        return exhibitionDao.deleteImageById(id);
    }
}

