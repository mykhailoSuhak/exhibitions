package com.exhibitions.service.exhibition;

import com.exhibitions.model.Exhibition;
import com.exhibitions.util.Page;

import java.io.InputStream;
import java.util.List;

public interface ExhibitionService {
    /**
     * This method finds exhibition by id.
     *
     * @param id Id of the exhibition to find.
     * @return Exhibition object, null if nothing is found.
     */
    Exhibition findExhibitionById(Long id, String locale);

    /**
     * This method returns n latest added exhibitions.
     *
     * @param limit How much exhibitions to find.
     * @return A list of exhibitions.
     */
    List<Exhibition> findLatestAdded(Integer limit, String locale);

    /**
     * This method returns a page of exhibitions with category id.
     *
     * @param page       Number of the page, starts from 1.
     * @param size       Size of the page.
     * @param categoryId Id of the category to filter exhibitions.
     * @return A list of exhibitions.
     */
    Page<Exhibition> getPageByCategoryId(Integer page, Integer size, Long categoryId, String locale);

    /**
     * This method returns a page of exhibitions by time.
     *
     * @param page      Number of the page, starts from 1.
     * @param size      Size of the page.
     * @param startdate Start date to filter exhibitions.
     * @param enddate   End date to filter exhibitions.
     * @return          A list of exhibitions.
     */
    Page<Exhibition> getPageByTime(Integer page, Integer size, String startdate, String enddate, String locale);

    /**
     * This method returns a page of exhibitions by price.
     *
     * @param page      Number of the page, starts from 1.
     * @param size      Size of the page.
     * @param direction   If true - sort by ASC, false - by DESC.
     * @return          A list of exhibitions.
     */
    Page<Exhibition> getPageByPrice(Integer page, Integer size, Boolean direction, String locale);

    /**
     * This method returns a page of all exhibitions.
     *
     * @param page Number of the page, starts from 1.
     * @param size Size of the page.
     * @return A list of exhibitions.
     */
    Page<Exhibition> getPage(Integer page, Integer size, String locale);

    /**
     * This method creates new exhibition.
     *
     * @param exhibition Exhibition object to create.
     * @return Updated object
     */
    Exhibition createExhibition(Exhibition exhibition, InputStream image);

    /**
     * This method updates exhibition.
     *
     * @param exhibition Exhibition object to update.
     * @return Updated object.
     */
    Exhibition updateExhibition(Exhibition exhibition, InputStream image);

    /**
     * This method deletes exhibition.
     *
     * @param id Id of the exhibition to delete.
     * @return True if exhibition deleted successfully, otherwise false.
     */
    boolean deleteExhibitionById(Long id);

    /**
     * This methods finds page from all exhibitions by name.
     *
     * @param query Search query.
     * @param page  Number of the page, starts from 1.
     * @param size  Size of the page.
     * @return List of exhibitions.
     */
    Page<Exhibition> getPageByName(String query, Integer page, Integer size, String locale);

    /**
     * This method finds image by id.
     *
     * @param id Id of the image to find.
     * @return   Image bytes.
     */
    byte[] findImageById(Long id);

    /**
     * This method crates new image.
     *
     * @param image Image stream.
     * @return      Created image id.
     */
    Long createImage(InputStream image);

    /**
     * This method deletes image by id.
     *
     * @param id Id of the image to delete.
     * @return   True if image deleted, otherwise false.
     */
    boolean deleteImageById(Long id);
}