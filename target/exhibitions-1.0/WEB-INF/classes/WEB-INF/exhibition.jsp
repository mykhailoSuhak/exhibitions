<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="navbar"%>
<%@ taglib uri="/WEB-INF/price.tld" prefix="p"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <%--Localization--%>
    <c:if test="${sessionScope.locale == null}">
        <fmt:setLocale value="en"/>
    </c:if>
    <c:if test="${sessionScope.locale != null}">
        <fmt:setLocale value="${sessionScope.locale}"/>
    </c:if>

    <fmt:setBundle basename="localization" var="bundle"/>
    <%----%>

    <title>Exhibitions - ${exhibition.title}</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
</head>

<body style="padding-top:80px">

<navbar:navbar/>

<!-- Page Content -->
<div class="container">

    <div class="row">

        <div class="col-md-3">
            <p class="lead"><fmt:message key="categories" bundle="${bundle}"/></p>
            <div class="list-group">
                <c:forEach items="${categories}" var="category">
                    <a href="${pageContext.request.contextPath}/category?catId=${category.id}&p=1&s=6"
                       class="list-group-item">${category.name}</a>
                </c:forEach>
            </div>
            <form class="form-horizontal" method="post"
                  action="${pageContext.request.contextPath}/byTime?p=1&s=6"
                  enctype="multipart/form-data">

                <legend><fmt:message key="sortByTime" bundle="${bundle}"/></legend>
                <div class="form-group">
                    <label class="col-md-3 control-label" for="title">
                        <fmt:message key="from" bundle="${bundle}"/>
                    </label>
                    <div class="col-md-8">
                        <input type="date" id="startdate" name="startdate" align="right"/>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-label" for="title">
                        <fmt:message key="to" bundle="${bundle}"/>
                    </label>
                    <div class="col-md-8">
                        <input type="date" id="enddate" name="enddate" align="right"/>
                    </div>
                </div>

                <div class="col-md-2">
                    <button id="submit" name="submit" type="submit" class="btn btn-primary">
                        <fmt:message key="filter" bundle="${bundle}"/>
                    </button>
                </div>

            </form>
        </div>

        <div class="col-md-6">

            <div class="thumbnail">
                <div class="caption-full">
                    <h4 class="pull-right"><p:price price="${exhibition.price}"/></h4>
                    <h3>${exhibition.title}</h3>

                    <p>
                        <b><fmt:message key="time" bundle="${bundle}"/>: </b><a>${exhibition.startDate} - ${exhibition.endDate}</a>
                    </p>
                    <p>
                        <b><fmt:message key="halls" bundle="${bundle}"/>: </b>
                        <c:forEach items="${exhibition.halls}" var="hall">
                        <a>${hall.title} </a>
                        </c:forEach>
                    </p>
                    <p>
                        <b><fmt:message key="category" bundle="${bundle}"/>: </b>
                        <a href="${pageContext.request.contextPath}/category?catId=${exhibition.category.id}&p=1&s=6">${exhibition.category.name}</a>
                    </p>

                    <div class="text-right">
                        <%--User logged in--%>
                        <c:if test="${sessionScope.authenticated != null &&
                                  sessionScope.authenticated == true &&
                                  sessionScope.role == 'USER'}">
                            <c:if test="${!isSubscribed}">
                                <a class="btn btn-primary" href="${pageContext.request.contextPath}/buyTicket?id=${exhibition.id}">
                                    <fmt:message key="buyTicket" bundle="${bundle}"/>
                                </a>
                            </c:if>

                            <c:if test="${isSubscribed}">
                                <button class="btn btn-success" disabled="disabled">
                                    <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
                                    <fmt:message key="ticketBought" bundle="${bundle}"/>
                                </button>
                            </c:if>
                        </c:if>



                        <%--User is admin--%>
                        <c:if test="${sessionScope.authenticated != null &&
                                  sessionScope.authenticated == true &&
                                  sessionScope.role == 'ADMIN'}">
                            <a class="btn btn-primary" href="${pageContext.request.contextPath}/admin/exhibitions/edit?id=${exhibition.id}">
                                <span class="glyphicon glyphicon-wrench" aria-hidden="true"></span>
                                <fmt:message key="edit" bundle="${bundle}"/>
                            </a>
                            <p></p>
                            <p>
                                <b> <fmt:message key="numberOfSoldTickets" bundle="${bundle}"/> </b> : ${count}
                            </p>
                        </c:if>

                        <%--User not logged in--%>
                        <c:if test="${sessionScope.authenticated == null }">
                            <a class="btn btn-primary " href="${pageContext.request.contextPath}/login">
                                <fmt:message key="buyTicket" bundle="${bundle}"/>
                            </a>
                        </c:if>
                    </div>

                    <div>
                        <hr>
                        <h5><b><fmt:message key="description" bundle="${bundle}"/></b></h5>

                        <c:if test="${exhibition.description != ''}">
                            <p>${exhibition.description}</p>
                        </c:if>
                        <c:if test="${exhibition.description == ''}">
                            <p><fmt:message key="noDescription" bundle="${bundle}"/></p>
                        </c:if>
                    </div>
                </div>
            </div>

        </div>

        <div class="col-md-3">
            <a href="#" class="thumbnail">

                    <img src="${pageContext.request.contextPath}/image?id=${exhibition.id}"/>


            </a>
        </div>

    </div>

</div>
<!-- /.container -->

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
        integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

</body>
</html>